/**
 * This utility class is largely based on:
 * https://github.com/jeroenheijmans/advent-of-code-2018/blob/master/AdventOfCode2018/Util.cs
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using Newtonsoft.Json;

namespace AdventOfCode.Solutions
{
    public static class Utilities {

        public static void TestPerformance(Action<object[]> f, int repeatCount, params object[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < repeatCount; i++) { f(args); }
            sw.Stop();
            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        public static string ToJson(object input) {
            return JsonConvert.SerializeObject(input, Formatting.Indented);
        }

        public static int[] ToIntArray(this string str, string delimiter = "") {
            return str
                .Split(delimiter)
                .Select(n => int.Parse(n))
                .ToArray();
        }

        public static long[] ToLongArray(this string str, string delimiter = "")
        {
            return str
                .Split(delimiter)
                .Select(n => long.Parse(n))
                .ToArray();
        }

        public static int MinOfMany(params int[] items) {
            var result = items[0];
            for (int i = 1; i < items.Length; i++) {
                result = Math.Min(result, items[i]);
            }
            return result;
        }

        public static int MaxOfMany(params int[] items) {
            var result = items[0];
            for (int i = 1; i < items.Length; i++) {
                result = Math.Max(result, items[i]);
            }
            return result;
        }

        // https://stackoverflow.com/a/3150821/419956 by @RonWarholic
        public static IEnumerable<T> Flatten<T>(this T[,] map) {
            for (int row = 0; row < map.GetLength(0); row++) {
                for (int col = 0; col < map.GetLength(1); col++) {
                    yield return map[row, col];
                }
            }
        }

        public static IEnumerable<IEnumerable<T>> Transpose<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(t => t.Select(
                    (tv, i) => new KeyValuePair<int, T>(i, tv)))
                .GroupBy(fv => fv.Key)
                .Select(g => g.Select(v => v.Value));
        }

        public static IEnumerable<IEnumerable<T>> Rotate<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany((t, y) => t.Select(
                    (tv, i) => new KeyValuePair<int, KeyValuePair<int, T>>(y, new KeyValuePair<int, T>(i, tv))))
                .GroupBy(fv => fv.Value.Key)
                .Select(g => g.OrderByDescending(k => k.Key).Select(v => v.Value.Value));
        }

        public static IEnumerable<IEnumerable<T>> FlipOnX<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.Reverse();
        }

        public static IEnumerable<IEnumerable<T>> FlipOnY<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.Select(row => row.Reverse());
        }


        public static string JoinAsStrings<T>(this IEnumerable<T> items, string delimiter = "") {
            return string.Join(delimiter, items);
        }

        public static string[] SplitByNewline(this string input, bool shouldTrim = false, bool shouldRemoveEmptyEntries = false) {
            return input
                .Split(new[] {"\r", "\n", "\r\n"}, StringSplitOptions.RemoveEmptyEntries)
                .Where(s => !shouldRemoveEmptyEntries || !string.IsNullOrWhiteSpace(s))
                .Select(s => shouldTrim ? s.Trim() : s)
                .ToArray();
        }
    }
}