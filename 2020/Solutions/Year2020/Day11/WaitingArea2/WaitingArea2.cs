﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020.WaitingArea2
{
    internal class WaitingArea2 : IWaitingArea
    {
        private IEnumerable<Seat> mSeats;
        private HashSet<string> mOccupied = new HashSet<string>();

        public WaitingArea2(string[] seats)
        {
            mSeats = seats
                .SelectMany((row, y) => row.Select((seat, x) => seat == 'L' ? new Seat(x, y) : null))
                .Where(s => s != null)
                .ToArray();
            foreach(var seat in mSeats)
            {
                seat.SetNeighbours(mSeats);
            }
        }

        public HashSet<string> Tick()
        {
            var newOccupied = new HashSet<string>();
            foreach(var seat in mSeats)
            {
                if(mOccupied.Contains(seat.Key))
                {
                    var staysOccupied = seat.Neighbours.Where(n => mOccupied.Contains(n)).Count() <= 4;
                    if(staysOccupied) newOccupied.Add(seat.Key);
                }
                else
                {
                    var getsOccupied = !seat.Neighbours.Any(n => mOccupied.Contains(n));
                    if(getsOccupied) newOccupied.Add(seat.Key);
                }
            }
            mOccupied = newOccupied;
            return mOccupied;
        }
    }


}
