﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020.WaitingArea2
{
    internal class Seat
    {
        public int X { get; }
        public int Y { get; }
        public ISet<string> Neighbours { get; set; }
        public string Key { get;  }

        public Seat(int x, int y)
        {
            X = x;
            Y = y;
            Key = $"{x}x{y}";
        }

        public void SetNeighbours(IEnumerable<Seat> seats)
        {
            var row = seats.Where(s => s.Y == Y);
            var left = row.Where(s => s.X < X).OrderBy(s => s.X).LastOrDefault();
            var right = row.Where(s => s.X > X).OrderBy(s => s.X).FirstOrDefault();

            var col = seats.Where(s => s.X == X);
            var top = col.Where(s => s.Y < Y).OrderBy(s => s.Y).LastOrDefault();
            var bottom = col.Where(s => s.Y > Y).OrderBy(s => s.Y).FirstOrDefault();

            var diag1 = seats.Where(s => s.X - s.Y == X - Y);
            var tl = diag1.Where(s => s.X < X).OrderBy(s => s.X).LastOrDefault();
            var br = diag1.Where(s => s.X > X).OrderBy(s => s.X).FirstOrDefault();

            var diag2 = seats.Where(s => s.X + s.Y == X + Y);
            var tr = diag2.Where(s => s.X > X).OrderBy(s => s.X).FirstOrDefault();
            var bl = diag2.Where(s => s.X < X).OrderBy(s => s.X).LastOrDefault();

            Neighbours = new[] { left, right, top, bottom, tl, tr, bl, br }
                .Where(s => s != null)
                .Select(s => $"{s.X}x{s.Y}")
                .ToHashSet();
        }
    }


}
