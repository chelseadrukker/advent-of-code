﻿using System.Collections.Generic;

namespace AdventOfCode.Solutions.Year2020
{
    internal interface IWaitingArea
    {
        HashSet<string> Tick();
    }
}
