﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020.WaitingArea1
{
    internal class Seat
    {
        public int X { get; }
        public int Y { get; }
        public ISet<string> Neighbours { get; }
        public string Key { get;  }

        public Seat(int x, int y)
        {
            X = x;
            Y = y;
            Neighbours = GenerateNeighbours(x, y);
            Key = $"{x}x{y}";
        }

        private HashSet<string> GenerateNeighbours(int x, int y)
        {
            return new[] {
                $"{x-1}x{y-1}",
                $"{x}x{y-1}",
                $"{x+1}x{y-1}",
                $"{x-1}x{y}",
                $"{x+1}x{y}",
                $"{x-1}x{y+1}",
                $"{x}x{y+1}",
                $"{x+1}x{y+1}",
            }.ToHashSet();
        }
    }


}
