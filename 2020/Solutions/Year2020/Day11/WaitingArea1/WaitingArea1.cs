﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020.WaitingArea1
{
    internal class WaitingArea1 : IWaitingArea
    {
        private IEnumerable<Seat> mSeats;
        private HashSet<string> mOccupied = new HashSet<string>();

        public WaitingArea1(string[] seats)
        {
            mSeats = seats
                .SelectMany((row, y) => row.Select((seat, x) => seat == 'L' ? new Seat(x, y) : null))
                .Where(s => s != null);
        }

        public HashSet<string> Tick()
        {
            var newOccupied = new HashSet<string>();
            foreach(var seat in mSeats)
            {
                if(mOccupied.Contains(seat.Key))
                {
                    var staysOccupied = seat.Neighbours.Where(n => mOccupied.Contains(n)).Count() <= 3;
                    if(staysOccupied) newOccupied.Add(seat.Key);
                }
                else
                {
                    var getsOccupied = !seat.Neighbours.Any(n => mOccupied.Contains(n));
                    if(getsOccupied) newOccupied.Add(seat.Key);
                }
            }
            mOccupied = newOccupied;
            return mOccupied;
        }
    }
}
