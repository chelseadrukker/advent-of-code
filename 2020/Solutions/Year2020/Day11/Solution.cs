using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day11 : ASolution
    {

        public Day11() : base(11, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            var wa = new WaitingArea1.WaitingArea1(Input.SplitByNewline());
            return Solve(wa);
        }

        private string Solve(IWaitingArea wa)
        {
            var current = new HashSet<string>();
            var next = wa.Tick();
            while(!current.SetEquals(next))
            {
                current = next;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                next = wa.Tick();
                sw.Stop();
                Console.WriteLine("Elapsed = {0}", sw.Elapsed);
            }
            return next.Count().ToString();
        }

        protected override string SolvePartTwo()
        {
            var wa = new WaitingArea2.WaitingArea2(Input.SplitByNewline());
            return Solve(wa);
        }
    }


}
