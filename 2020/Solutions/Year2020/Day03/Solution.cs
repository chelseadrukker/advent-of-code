using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day03 : ASolution {

        public Day03() : base(3, 2020, "") {

        }

        protected override string SolvePartOne()
        {
            var lines = Input.SplitByNewline();
            int treeCount = SolutionForOneAngle(lines, 3, 1);
            return treeCount.ToString();
        }

        private int SolutionForOneAngle(string[] lines, int x, int y)
        {
            var lineLength = lines[0].Length;
            var position = 0;
            var treeCount = 0;
            for(int row = 0; row < lines.Length; row += y)
            {
                treeCount += lines[row][position] == '#' ? 1 : 0;
                position += x;
                position %= lineLength;
            }
            return treeCount;
        }

        protected override string SolvePartTwo() {
            var lines = Input.SplitByNewline();

            var angles = new[] {
                new { X = 1, Y = 1 },
                new { X = 3, Y = 1 },
                new { X = 5, Y = 1 },
                new { X = 7, Y = 1 },
                new { X = 1, Y = 2 },
            };

            return angles
                .Select(a => (long)SolutionForOneAngle(lines, a.X, a.Y))
                .Aggregate((a, b) => a * b)
                .ToString();
        }
    }
}
