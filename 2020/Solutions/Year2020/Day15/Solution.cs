using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day15 : ASolution {

        public Day15() : base(15, 2020, "") {

        }

        protected override string SolvePartOne() {
            var memory = new Dictionary<string, int[]>();
            var numbers = Input.Split(',', System.StringSplitOptions.RemoveEmptyEntries);
            for(int i = 1; i <= numbers.Length; i++)
            {
                memory[numbers[i-1]] = new[] { i };
            }
            var turn = numbers.Length + 1;
            var lastSpoken = numbers.Last();

            while(turn <= 2020) {
                var nextSpoken = memory[lastSpoken].Length > 1
                    ? (memory[lastSpoken][0] - memory[lastSpoken][1]).ToString()
                    : "0";
                memory[nextSpoken] = memory.ContainsKey(nextSpoken)
                    ? memory[nextSpoken].Prepend(turn).ToArray()
                    : new[] { turn };
                turn++;
                lastSpoken = nextSpoken;
            }
            return lastSpoken;
        }

        protected override string SolvePartTwo()
        {
            return SolveFast(30000000);
        }

        private string SolveFast(int limit)
        {
            var memory = new Dictionary<long, AB>();
            var numbers = Input.Split(',', System.StringSplitOptions.RemoveEmptyEntries);
            long turns = numbers.Length;
            for(int i = 1; i <= turns; i++)
            {
                memory[int.Parse(numbers[i - 1])] = new AB { A = i, B = null };
            }

            var lastSpoken = long.Parse(numbers.Last());
            while(turns++ < limit)
            {
                lastSpoken = memory[lastSpoken].B != null
                    ? memory[lastSpoken].Difference
                    : 0;
                if(memory.ContainsKey(lastSpoken))
                    memory[lastSpoken].Shift(turns);
                else
                    memory[lastSpoken] = new AB { A = turns, B = null };
            }

            return lastSpoken.ToString();
        }
    }

    internal class AB
    {
        public long A { get; set; }
        public long? B { get; set; }
        public long Difference => (long)(A - B);

        internal void Shift(long turn)
        {
            B = A;
            A = turn;
        }
    }
}
