using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020 {

    class Day17 : ASolution {
        protected override string SolvePartOne() {
            TestInput = Input;
            var input = TestInput.SplitByNewline();
            var cubes = input
                .SelectMany((r, y) => r.Select((c, x) => c == '#' ? new Cube(x, y, 0, 0) : null))
                .Where(c => c != null)
                .ToArray();
            var dimension = input.Length;

            for(int i = 1; i <= 6; i++)
            {
                var nextEvolution = new List<Cube>();
                for(int x = 0 - i; x < dimension + i; x++)
                    for(int y = 0 - i; y < dimension + i; y++)
                        for(int z = 0 - i; z < 1 + i; z++)
                            for(int w = 0 - i; w < 1 + i; w++)
                            {
                                var cube = new Cube(x, y, z, w);
                                var activeNeighbours = cubes.Where(c => cube.Neighbours.Contains(c)).Count();
                                if(cubes.FirstOrDefault(c => c.Equals(cube)) != null
                                    && activeNeighbours >= 2
                                    && activeNeighbours <= 3)
                                {
                                    nextEvolution.Add(cube);
                                    continue;
                                } else if(cubes.FirstOrDefault(c => c.Equals(cube)) == null
                                      && activeNeighbours == 3) {
                                    nextEvolution.Add(cube);
                                    continue;
                                }
                            }
                cubes = nextEvolution.ToArray();
                Console.WriteLine(i);
            }
            return Utilities.ToJson(cubes.Length);
        }

        protected override string SolvePartTwo() {
            //TestInput = Input;
            var result = TestInput.SplitByNewline();
            return Utilities.ToJson("");
        }

        public Day17() : base(17, 2020, "")
        {
            TestInput = @".#.
..#
###";
        }

        public string TestInput { get; set; }
    }
    internal class Cube : Position
    {
        public Cube(int x, int y, int z, int w) : base(x, y, z, w)
        {
            Neighbours = GenerateNeighbours(x, y, z, w).Where(p => !p.Equals(this)).ToArray();
        }

        private IEnumerable<Position> GenerateNeighbours(int x, int y, int z, int w)
        {
            for(int xt = x - 1; xt <= x + 1; xt++)
                for(int yt = y - 1; yt <= y + 1; yt++)
                    for(int zt = z - 1; zt <= z + 1; zt++)
                        for(int wt = w - 1; wt <= w + 1; wt++)
                            yield return new Position(xt, yt, zt, wt);

        }

        public Position[] Neighbours { get; }
    }

    internal class Position
    {
        public override bool Equals(object obj)
        {
            return X == ((Cube)obj).X
            && Y == ((Cube)obj).Y
            && Z == ((Cube)obj).Z
            && W == ((Cube)obj).W;
        }
        public Position(int x, int y, int z, int w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public int X { get; }
        public int Y { get; }
        public int Z { get; }
        public int W { get; }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
