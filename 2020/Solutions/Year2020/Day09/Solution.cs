using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day09 : ASolution {

        public Day09() : base(9, 2020, "") {

        }

        protected override string SolvePartOne()
        {
            var input = Input.SplitByNewline().Select(l => long.Parse(l));
            return StepRecursive(input, 0).ToString();
        }

        private static long StepRecursive(IEnumerable<long> input, int step)
        {
            var keys = input.Skip(step).Take(25);
            var sums = keys.SelectMany(k => keys.Where(k2 => k2 != k).Select(k3 => k3 + k));
            var check = input.Skip(step + 25).First();
            return sums.Contains(check) ? StepRecursive(input, step + 1) : check;
        }

        protected override string SolvePartTwo() {
            var input = Input.SplitByNewline().Select(l => long.Parse(l)).ToHashSet();
            var sum = long.Parse(SolvePartOne());
            var size = input.Count();
            for(int groupsize = 2; groupsize < size-1; groupsize++)
            {
                for(int position = 0; position <= size - groupsize; position++)
                {
                    var candidate = input.Skip(position).Take(groupsize);
                    if(candidate.Sum() == sum)
                        return (candidate.Min() + candidate.Max()).ToString();
                }
            }
            return "Result not found.";
        }

        private (long, long) FindResultsWithHashSet(ISet<long> numbers, long expectedSum)
        {
            foreach(long n in numbers)
            {
                long remainder = expectedSum - n;
                if(numbers.Contains(remainder))
                    return (remainder, n);
            }
            return (-1, -1);
        }
    }
}
