using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    class Day21 : ASolution
    {
        protected override string SolvePartOne()
        {
            var foods = Foods();
            var matches = Matches(foods);
            var ingredients = foods.SelectMany(f => f.Ingredients);
            return ingredients
                .Where(i => !matches.Values.Contains(i))
                .Count()
                .ToString();
        }

        protected override string SolvePartTwo()
        {
            return Matches(Foods())
                .OrderBy(m => m.Key)
                .Select(m => m.Value)
                .JoinAsStrings(",");
        }

        private IEnumerable<Food> Foods()
        {
            var rows = Input.Split("\n");
            var foods = rows
                .Select(r => Regex.Match(r, @"([\w\s]+)\s(\(contains\s(.+)\))?"))
                .Select(m => FoodFactory(m));
            return foods;
        }

        private Food FoodFactory(Match m)
        {
            var ingredients = m.Groups[1].Value.Split(" ");
            var allergens = m.Groups[3].Value.Split(",", StringSplitOptions.TrimEntries);
            return new Food(ingredients, allergens);
        }

        private Dictionary<string, string> Matches(IEnumerable<Food> foods)
        {
            var allergens = foods.SelectMany(f => f.Allergens).Distinct();

            var candidates = allergens.Select(a => (Allergen: a, Ingredients: foods.Where(f => f.Allergens.Contains(a)).Select(f => f.Ingredients).Aggregate((a, b) => a.Intersect(b)).ToList())).ToList();
            var matches = new Dictionary<string, string>();
            while(candidates.Any(c => c.Ingredients.Any()))
            {
                var match = candidates.First(c => c.Ingredients.Count() == 1);
                var ingredient = match.Ingredients.First();
                matches[match.Allergen] = ingredient;
                candidates.ForEach((c) => c.Ingredients.Remove(ingredient));
            }

            return matches;
        }

        public Day21() : base(21, 2020, "")
        {
        }
    }

    internal static class Test21
    {
        internal static string Input = @"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)";
    }
}