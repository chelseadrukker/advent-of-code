﻿using System.Collections.Generic;

namespace AdventOfCode.Solutions.Year2020
{
    public class Food
    {
        public IEnumerable<string> Ingredients { get; }
        public IEnumerable<string> Allergens { get; }

        public Food(IEnumerable<string> ingredients, IEnumerable<string> allergens)
        {
            Ingredients = ingredients;
            Allergens = allergens;
        }
    }
}