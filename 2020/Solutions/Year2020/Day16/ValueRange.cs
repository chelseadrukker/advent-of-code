﻿namespace AdventOfCode.Solutions.Year2020
{
    public class ValueRange
    {
        private readonly int mStart;
        private readonly int mEnd;

        public ValueRange(string start, string end)
        {
            mStart = int.Parse(start);
            mEnd = int.Parse(end);
        }

        internal bool Contains(int i)
        {
            return mStart <= i && i <= mEnd;
        }
    }
}
