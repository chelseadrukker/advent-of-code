﻿using System.Collections.Generic;

namespace AdventOfCode.Solutions.Year2020
{
    public class FieldValues
    {
        public FieldValues(int fieldIndex, ISet<int> values)
        {
            Key = fieldIndex;
            Values = values;
        }

        public int Key { get; }
        public ISet<int> Values { get; }
    }
}
