﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    public class FieldWithCandidates
    {
        public string Name;
        public List<int> Candidates;

        public bool Unresolved { get; private set; }

        public FieldWithCandidates(string name, IEnumerable<int> candidates)
        {
            Name = name;
            Candidates = candidates.ToList();
            Unresolved = true;
        }

        public int Resolve() {
            Unresolved = false;
            return Candidates.First();
        }

        internal void RemoveCandidate(int x)
        {
            Candidates.Remove(x);
        }
    }
}
