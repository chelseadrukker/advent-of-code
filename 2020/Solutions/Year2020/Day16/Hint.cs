﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class Hint
    {
        public Hint(string hint)
        {
            var match = Regex.Match(hint, @"(?<name>.+): (?<start1>\d+)-(?<end1>\d+)\sor\s(?<start2>\d+)-(?<end2>\d+)");
            Name = match.Groups["name"].Value;
            Ranges = new ValueRange[] {
                new ValueRange(match.Groups["start1"].Value, match.Groups["end1"].Value),
                new ValueRange(match.Groups["start2"].Value, match.Groups["end2"].Value),
            };
        }

        public string Name { get; private set; }
        public ValueRange[] Ranges { get; }

        internal bool Contains(int i)
        {
            return Ranges.Any(r => r.Contains(i));
        }

        public IEnumerable<int> Candidates(IEnumerable<FieldValues> fieldValues)
        {
            return fieldValues
                .Where(fv => fv.Values.All(v => Ranges.Any(r => r.Contains(v))))
                .Select(fv => fv.Key);
        }
    }
}
