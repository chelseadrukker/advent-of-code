using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day16 : ASolution
    {
        private readonly string[] mHints;
        private readonly string[] mNearbyTickets;
        private readonly string mMyTicket;

        public Day16() : base(16, 2020, "")
        {
            var split = Input.Split("\n\n");
            mHints = split[0].SplitByNewline();
            mMyTicket = split[1].SplitByNewline().Skip(1).First();
            mNearbyTickets = split[2].SplitByNewline().Skip(1).ToArray();
        }

        protected override string SolvePartOne()
        {
            var hints = mHints.Select(hint => new Hint(hint));
            var nearbyValues = mNearbyTickets.Select(l => l.ToIntArray(","));

            return nearbyValues
                .SelectMany(v => v)
                .Where(i => !hints.Any(h => h.Contains(i)))
                .Sum()
                .ToString();
        }

        protected override string SolvePartTwo()
        {
            var hints = mHints.Select(l => new Hint(l));

            var nearbyFieldValues = mNearbyTickets
                .Select(t => t.ToIntArray(","))
                .Where(t => t.All(v => hints.Any(h => h.Contains(v))))
                .Transpose()
                .Select((values, i) => new FieldValues(i, values.ToHashSet()));

            var fieldsWithCandidates = ComputeResult(hints, nearbyFieldValues);

            var indices = fieldsWithCandidates
                .Where(s => s.Name.StartsWith("departure"))
                .Select(s => s.Candidates.First());

            return mMyTicket
                .ToLongArray(",")
                .Where((v, i) => indices.Contains(i))
                .Aggregate((a, b) => a * b)
                .ToString();
        }

        private static List<FieldWithCandidates> ComputeResult(IEnumerable<Hint> hints, IEnumerable<FieldValues> nearbyFieldValues)
        {
            var fieldsWithCandidates = hints
                            .Select(h => new FieldWithCandidates(h.Name, h.Candidates(nearbyFieldValues)))
                            .ToList();

            while(fieldsWithCandidates.Any(f => f.Unresolved))
            {
                var x = fieldsWithCandidates.Where(f => f.Unresolved)
                    .First(f => f.Candidates.Count() == 1)
                    .Resolve();
                foreach(var fc in fieldsWithCandidates.Where(f => f.Unresolved))
                    fc.RemoveCandidate(x);
            }

            return fieldsWithCandidates;
        }
    }
}
