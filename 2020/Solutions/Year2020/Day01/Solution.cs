using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day01 : ASolution {

        public Day01() : base(1, 2020, "") {

        }

        protected override string SolvePartOne()
        {
            int[] numbers = Input.SplitByNewline().Select(v => int.Parse(v)).ToArray();
            return FindResultsWithHashSet(numbers.ToHashSet(), 2020).ToString();
        }

        private int? FindResultsWithHashSet(ISet<int> numbers, int expectedSum) {
            foreach (int n in numbers) {
                int remainder = expectedSum - n;
                if (numbers.Contains(remainder))
                    return remainder * n;
            }
            return null;
        }

        private int? FindResultFromTwoNumbers(IEnumerable<int> numbers, int expectedSum)
        {
            var under = numbers.Where(n => n <= expectedSum / 2);
            var over = numbers.Where(n => n > expectedSum / 2).ToHashSet();

            if (!under.Any() || !over.Any())
                return null;

            foreach (var u in under)
            {
                int candidate = expectedSum - u;
                var found = over.Contains(candidate);
                if (found)
                    return u * candidate;
            }
            return null;
        }

        protected override string SolvePartTwo()
        {
            //Utilities.TestPerformance(args => {
            //    int number = (int)args[0];
            //    PartTwo(number);
            //}, 10000, Year);

            return PartTwo(Year);
        }

        private string PartTwo(int expectedSum)
        {
            var numbers = Input.SplitByNewline().Select(v => int.Parse(v)).ToHashSet();
            foreach (var n in numbers)
            {
                int remaining = expectedSum - n;
                var others = numbers.Except(new[] { n });
                var result = FindResultsWithHashSet(others.ToHashSet(), remaining);
                if (result != null)
                    return (result.Value * n).ToString();
            }
            return null;
        }
    }
}
