using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{

    class Day18 : ASolution
    {
        Regex mRegex = new Regex(@"(\([\d\s\+\*]+\))");

        protected override string SolvePartOne()
        {
            var expressions = Input.SplitByNewline();
            var result = expressions.Select(e => Simplify(e, EvaluateSequentially)).Sum();
            return Utilities.ToJson(result);
        }

        private long Simplify(string expression, Func<string, string> evaluate)
        {
            var matches = mRegex.Matches(expression);
            while(matches.Any()) {
                foreach(Match match in matches)
                {
                    expression = expression.Replace(match.Value, evaluate(match.Value));
                }
                matches = mRegex.Matches(expression);
            }
            return long.Parse(evaluate(expression));
        }

        private string EvaluateSequentially(string expression)
        {
            var numsAndOps = expression
                .TrimStart('(')
                .TrimEnd(')')
                .Split(" ");
            var result = long.Parse(numsAndOps.First());
            for(int i = 1; i < numsAndOps.Length; i += 2)
            {
                var val = long.Parse(numsAndOps[i + 1]);
                result = numsAndOps[i] == "+"
                    ? result + val
                    : result * val;
            }
            return result.ToString();
        }

        private string EvaluateAddBeforeMul(string expression)
        {
            var numsAndOps = expression
                .TrimStart('(')
                .TrimEnd(')')
                .Split(" ");
            var sum = long.Parse(numsAndOps.First());
            var toMultiply = new List<long>();
            for(var i = 1; i < numsAndOps.Length; i += 2)
            {
                var val = long.Parse(numsAndOps[i + 1]);
                if(numsAndOps[i] == "+")
                    sum += val;
                else {
                    toMultiply.Add(sum);
                    sum = val;
                }
            }
            toMultiply.Add(sum);
            return toMultiply.Aggregate((a, b) => a * b).ToString();
        }

        protected override string SolvePartTwo()
        {
            var expressions = Input.SplitByNewline();
            var result = expressions.Select(e => Simplify(e, EvaluateAddBeforeMul)).Sum();
            return Utilities.ToJson(result);
        }

        public Day18() : base(18, 2020, "")
        {
        }
    }
}