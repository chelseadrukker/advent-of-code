﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Player
    {
        public Player(string player)
        {
            Name = player.SplitByNewline().First().TrimEnd(':');
            Deck = player.SplitByNewline().Skip(1).Select(l => int.Parse(l));
            CardPlayed = Deck.FirstOrDefault();
            HasEnoughCardsForSubRound = CardPlayed <= Deck.Count() - 1;
        }

        public Player(string name, IEnumerable<int> deck)
        {
            Name = name;
            Deck = deck;
            CardPlayed = deck.FirstOrDefault();
            HasEnoughCardsForSubRound = CardPlayed <= deck.Count() - 1;
        }

        public string Name { get; }
        public IEnumerable<int> Deck { get; }
        public bool HasEnoughCardsForSubRound { get; }

        internal Player PlayerForSubGame()
        {
            return new Player(Name, Deck.Skip(1).Take(Deck.First()));
        }

        internal int CardPlayed { get; }

        internal Player Win(int winningCard, int loosingCard)
        {
            return new Player(Name, Deck.Skip(1).Append(winningCard).Append(loosingCard));
        }

        internal Player Loose()
        {
            return new Player(Name, Deck.Skip(1));
        }
    }
}