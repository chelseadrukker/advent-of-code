﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Round
    {
        private IEnumerable<Player> mPlayers;
        public Player Player1 => mPlayers.First(p => p.Name == "Player 1");

        public Round(IEnumerable<Player> players)
        {
            mPlayers = players;
        }

        internal RoundResult Play()
        {
            if(mPlayers.All(p => p.HasEnoughCardsForSubRound))
            {
                var roundWinner = new Game(mPlayers.Select(p => p.PlayerForSubGame())).Play();
                mPlayers = mPlayers.OrderByDescending(p => p.Name == roundWinner.Name);
            }
            else
                mPlayers = mPlayers.OrderByDescending(p => p.CardPlayed);

            var winner = mPlayers.First();
            var winningCard = winner.CardPlayed;
            var looser = mPlayers.Last();
            var loosingCard = looser.CardPlayed;
            var nexRoundPlayers = new List<Player>() {
                winner.Win(winningCard, loosingCard),
                looser.Loose()
            };
            return new RoundResult(nexRoundPlayers);
        }
    }
}