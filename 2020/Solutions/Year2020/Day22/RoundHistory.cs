﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class RoundHistory
    {
        private Dictionary<string, string> mMemory;
        public RoundHistory()
        {
            mMemory = new Dictionary<string, string>();
        }

        internal bool TryAdd(IEnumerable<Player> players)
        {
            var ordered = players.OrderBy(p => p.Name);
            return mMemory.TryAdd(ordered.First().Deck.JoinAsStrings(","), ordered.Last().Deck.JoinAsStrings(","));
        }

        internal void Add(IEnumerable<Player> players)
        {
            var ordered = players.OrderBy(p => p.Name);
            mMemory.Add(ordered.First().Deck.JoinAsStrings(","), ordered.Last().Deck.JoinAsStrings(","));
        }
    }
}