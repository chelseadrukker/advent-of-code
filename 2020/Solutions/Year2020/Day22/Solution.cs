using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    class Day22 : ASolution
    {
        protected override string SolvePartOne()
        {
            var players = Input.Split("\n\n")
                .Select(player => player.SplitByNewline().Skip(1).Select(l => int.Parse(l)));

            while(players.All(p => p.Count() > 0))
            {
                players = players.OrderByDescending(p => p.First());
                var winner = players.First().First();
                var looser = players.Last().First();
                players = new List<IEnumerable<int>>() {
                    players.First()
                        .Skip(1)
                        .Append(winner)
                        .Append(looser),
                    players.Last()
                        .Skip(1)
                };
            }

            return Utilities.ToJson(players
                .First()
                .Reverse()
                .Select((n, i) => n * (i + 1))
                .Sum());
        }

        protected override string SolvePartTwo()
        {
            //var players = Test22.Input.Split("\n\n")
            //var players = Test22Rec.Input.Split("\n\n")
            var players = Input.Split("\n\n")
                .Select(player => new Player(player));

            var winner = new Game(players).Play();

            return Utilities.ToJson(winner
                .Deck
                .Reverse()
                .Select((n, i) => n * (i + 1))
                .Sum());
        }

        public Day22() : base(22, 2020, "")
        {
        }
    }

    internal static class Test22
    {
        internal static string Input = @"Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10".Replace("\r", string.Empty);
    }

    internal static class Test22Rec
    {
        internal static string Input = @"Player 1:
12
43
19
39
13
50
7

Player 2:
49
4
29
18
46
11
41
9
38
24".Replace("\r", string.Empty);
    }
}