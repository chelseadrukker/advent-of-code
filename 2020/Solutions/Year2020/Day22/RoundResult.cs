﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class RoundResult
    {
        public IEnumerable<Player> Players { get; }

        public RoundResult(IEnumerable<Player> players)
        {
            Players = players;
        }

        public Player Winner => Players.All(p => p.Deck.Any()) ? null : Players.First(p => p.Deck.Any());
    }
}