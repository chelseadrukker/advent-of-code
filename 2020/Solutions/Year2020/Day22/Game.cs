﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Game
    {
        private IEnumerable<Player> mPlayers;

        public Game(IEnumerable<Player> players)
        {
            mPlayers = players;
        }

        internal Player Play()
        {
            var history = new RoundHistory();
            history.Add(mPlayers);
            var round = new Round(mPlayers);
            var result = round.Play();
            while(result.Winner == null)
            {
                if(!history.TryAdd(result.Players)) {
                    return round.Player1;
                }

                round = new Round(result.Players);
                result = round.Play();
            }
            return result.Winner;
        }
    }
}