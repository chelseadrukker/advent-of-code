using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{

    class Day19 : ASolution
    {
        protected override string SolvePartOne()
        {
            var result = Input.Split($"\n\n");
            var rules = result[0]
                .SplitByNewline()
                .Select(l => Regex.Match(l, @"^(\d+):\s(.*)$").Groups)
                .ToDictionary(groups => $"x{groups[1].Value}x",
                    groups => $"x{groups[2].Value.Replace("\"", "").Replace(" ", "xx")}x");

            var rule = "^" + rules["x0x"] + "$";
            var matches = Regex.Matches(rule, @"x\d+x");
            while(matches.Any())
            {
                foreach(var matchedValue in matches.Select(m => m.Value).Distinct())
                {
                    rule = rule.Replace(matchedValue, "(" + rules[matchedValue] + ")");
                }
                matches = Regex.Matches(rule, @"x\d+x");
            }
            var messages = result[1].SplitByNewline();
            var cleanRule = rule.Replace(" ", "").Replace("x", "");
            return Utilities.ToJson(messages.Where(m => Regex.IsMatch(m, cleanRule)).Count());
        }

        protected override string SolvePartTwo()
        {
            var result = Input.Split($"\n\n");
            var rules = result[0]
                .SplitByNewline()
                .Select(l => Regex.Match(l, @"^(\d+):\s(.*)$").Groups)
                .ToDictionary(groups => $"x{groups[1].Value}x",
                    groups => $"x{groups[2].Value.Replace("\"", "").Replace(" ", "xx")}x");

            rules["x8x"] = "x42x|x42xx8x";
            rules["x11x"] = "x42xx31x|x42xx11xx31x";

            var rule = "^" + rules["x0x"] + "$";
            var matches = Regex.Matches(rule, @"x\d+x");
            var oldCount = 0;
            var count = 0;
            var messages = result[1].SplitByNewline();
            while(count == 0 || oldCount != count)
            {
                var matchedValues = matches.Select(m => m.Value).Distinct();
                foreach(var matchedValue in matchedValues)
                {
                    rule = rule.Replace(matchedValue, "(" + rules[matchedValue] + ")");
                }
                matches = Regex.Matches(rule, @"x\d+x");
                var cleanRule = rule.Replace(" ", "").Replace("x", "");
                oldCount = count;
                count = messages.Where(m => Regex.IsMatch(m, cleanRule)).Count();
            }
            return Utilities.ToJson(count);
        }

        public Day19() : base(19, 2020, "")
        {
            TestInput = @"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: ""a""
5: ""b""

ababbb
bababa
abbbab
aaabbb
aaaabbb".Replace("\r", string.Empty);
        }

        public string TestInput { get; set; }
    }
}