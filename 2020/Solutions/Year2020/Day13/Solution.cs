using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day13 : ASolution {

        public Day13() : base(13, 2020, "") {

        }

        protected override string SolvePartOne() {
            var input = Input.SplitByNewline();
            var timestamp = int.Parse(input[0]);
            var bus = input[1]
                .Replace("x", string.Empty)
                .Split(",", System.StringSplitOptions.RemoveEmptyEntries)
                .Select(b => int.Parse(b))
                .Select(b => new { Bus = b, Wait = b - (timestamp % b) })
                .OrderBy(b => b.Wait)
                .First();
            return (bus.Bus * bus.Wait).ToString();
        }

        protected override string SolvePartTwo() {
            var buses = Input.SplitByNewline()[1]
                .Split(",")
                .Select((b, i) => long.TryParse(b, out var result) ? new Bus(result, i) : null)
                .Where(b => b != null);
            var agg = buses.Aggregate((a, b) =>
            {
                return a.MergeWith(b);
            });

            return agg.Offset.ToString();
        }

        protected string SolvePartTwoSlow()
        {
            var buses = Input.SplitByNewline()[1]
                .Split(",")
                .Select((b, i) => int.TryParse(b, out var result) ? new { Bus = result, Index = i } : null)
                .Where(b => b != null);
            var firstBus = buses.First();
            long current = firstBus.Index;

            while(true)
            {
                if(buses.All(b => (current + b.Index + firstBus.Index) % b.Bus == 0))
                    break;
                current += firstBus.Bus;
            }

            return current.ToString();
        }
    }
}
