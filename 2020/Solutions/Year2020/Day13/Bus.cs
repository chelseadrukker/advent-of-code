﻿namespace AdventOfCode.Solutions.Year2020
{
    public class Bus
    {
        public Bus(long step, long offset)
        {
            Step = step;
            Offset = offset;
        }

        public long Step { get; }
        public long Offset { get; }

        public Bus MergeWith(Bus b)
        {
            var newOffset = Offset;

            while((newOffset + b.Offset) % b.Step != 0)
            {
                newOffset += Step;
            }

            return new Bus(Step * b.Step, newOffset);
        }
    }
}
