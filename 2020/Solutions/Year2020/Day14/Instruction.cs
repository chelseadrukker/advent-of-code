﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Instruction
    {
        public Instruction(string line)
        {
            var match = Regex.Match(line, @"\w+\[(?<address>\d+)\]\s=\s(?<value>\w+)");
            mAddress = match.Groups["address"].Value;
            mValue = long.Parse(match.Groups["value"].Value);
        }

        private readonly string mAddress;
        private readonly long mValue;

        internal void Execute(string mMask, Dictionary<string, long> mMemory)
        {
            var paddedBinaryValue = Convert
                .ToString(int.Parse(mAddress), 2)
                .PadLeft(mMask.Length, '0');
            var address = string.Join("", mMask.Select((c, i) => c == '0' ? paddedBinaryValue[i] : c));
            var addresses = RecursiveAddresses(address);
            foreach(var a in addresses) {
                mMemory[a] = mValue;
            }
        }

        private IEnumerable<string> RecursiveAddresses(string address)
        {
            if(address.Contains('X'))
            {
                var index = address.IndexOf('X');
                var start = address.Substring(0, index);
                var end = address.Substring(index + 1, address.Length - index - 1);
                var address1 = $"{start}0{end}";
                var address2 = $"{start}1{end}";
                return RecursiveAddresses(address1).Concat(RecursiveAddresses(address2));
            }
            return new string[] { address };
        }
    }
}
