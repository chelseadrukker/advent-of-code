﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class PortComputer
    {
        private string[] mInput;

        public PortComputer(string input)
        {
            mInput = input.SplitByNewline();
        }

        public string Process()
        {
            var mask = "";
            var memory = new Dictionary<string, long>();
            foreach(var line in mInput)
            {
                var match = Regex.Match(line, @"(?<instruction>\w+)(?<index>\[\d+\])?\s=\s(?<value>\w+)");
                var instruction = match.Groups["instruction"].Value;
                var value = match.Groups["value"].Value;

                if(instruction == "mask")
                    mask = value;
                else
                    memory[match.Groups["index"].Value] = Mask(value, mask);
            }
            return memory.Values.Sum().ToString();
        }

        private long Mask(string value, string mask)
        {
            var paddedBinaryValue = Convert
                .ToString(int.Parse(value), 2)
                .PadLeft(mask.Length, '0');
            var masked = string.Join("", mask.Select((c, i) => c == 'X' ? paddedBinaryValue[i] : c));
            return Convert.ToInt64(masked, 2);
        }
    }
}