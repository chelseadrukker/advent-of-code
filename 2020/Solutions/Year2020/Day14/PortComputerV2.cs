﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class PortComputerV2
    {
        private string mInput;

        public PortComputerV2(string input)
        {
            mInput = input;
        }

        internal string Process()
        {
            var memory = new Dictionary<string, long>();
            var batches = mInput
                .Split("mask", StringSplitOptions.RemoveEmptyEntries)
                .Select(b => new Batch(b, memory));

            foreach(var batch in batches)
            {
                batch.Process();
            };

            return memory.Values.Sum().ToString();
        }
    }
}
