﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Batch
    {
        private readonly string mMask;
        private readonly IEnumerable<Instruction> mInstructions;
        private readonly Dictionary<string, long> mMemory;

        public Batch(string batch, Dictionary<string, long> memory)
        {
            var lines = batch.SplitByNewline();
            mMask = Regex.Match(lines.First(), @"\s=\s(?<value>\w+)").Groups["value"].Value;
            mInstructions = lines.Skip(1).Select(i => new Instruction(i));
            mMemory = memory;
        }

        public void Process() {
            foreach(var instruction in mInstructions)
            {
                instruction.Execute(mMask, mMemory);
            }
        }
    }
}
