namespace AdventOfCode.Solutions.Year2020
{

    class Day14 : ASolution {

        public Day14() : base(14, 2020, "") {

        }

        protected override string SolvePartOne() {
            return new PortComputer(Input).Process();
        }

        protected override string SolvePartTwo() {
            return new PortComputerV2(Input).Process();
        }
    }
}
