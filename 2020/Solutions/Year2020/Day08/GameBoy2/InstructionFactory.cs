﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020.GameBoy2
{
    internal class InstructionFactory
    {
        private string mInstruction;
        private readonly int mPosition;

        public InstructionFactory(string instruction, int position)
        {
            mInstruction = instruction;
            mPosition = position;
        }

        public IInstruction Create()
        {
            var match = Regex.Match(mInstruction, @"^(\w+) ((\+|-)\d+)$");
            var parameter = int.Parse(match.Groups[2].Value);
            var instructionType = match.Groups[1].Value;
            switch(instructionType)
            {
                case "nop":
                    return new NopJmp(1, parameter, mPosition);
                case "acc":
                    return new Acc(parameter, mPosition);
                case "jmp":
                    return new NopJmp(parameter, 1, mPosition);
                default:
                    throw new Exception("Instruction type not recognized: {instructionType}");
            }
        }
    }
}