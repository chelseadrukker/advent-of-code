﻿namespace AdventOfCode.Solutions.Year2020.GameBoy2
{
    public interface IInstruction
    {
        int ExecuteRecursively(IInstruction[] instructions, ref int acc, bool changeMade);
    }
}