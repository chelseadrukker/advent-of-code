﻿using System.Linq;

namespace AdventOfCode.Solutions.Year2020.GameBoy2
{
    internal partial class GameBoy
    {
        private readonly string mInput;

        public GameBoy(string input)
        {
            mInput = input;
        }

        internal string Solve()
        {
            var instructions = mInput.SplitByNewline().Select((instruction, position) => new InstructionFactory(instruction, position).Create()).ToArray();
            var acc = 0;
            instructions[0].ExecuteRecursively(instructions, ref acc, false);
            return acc.ToString();
        }
    }
}