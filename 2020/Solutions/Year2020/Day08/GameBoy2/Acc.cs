﻿namespace AdventOfCode.Solutions.Year2020.GameBoy2
{
    internal class Acc : IInstruction
    {
        private readonly int mValue;
        private readonly int mPosition;
        private bool mVisited = false;

        public Acc(int value, int position)
        {
            mValue = value;
            mPosition = position;
        }

        public int ExecuteRecursively(IInstruction[] instructions, ref int acc, bool changeMade)
        {
            if(!mVisited)
            {
                mVisited = true;
                acc += mValue;
                var move = mPosition + 1;

                if(move == instructions.Length || instructions[move].ExecuteRecursively(instructions, ref acc, changeMade) >= 0)
                {
                    return 1;
                }
                acc -= mValue;
            }
            mVisited = false;
            return -1;
        }
    }
}