﻿namespace AdventOfCode.Solutions.Year2020.GameBoy2
{
    internal class NopJmp : IInstruction
    {
        private readonly int mPrimaryJump;
        private readonly int mSecondaryJump;
        private readonly int mPosition;
        private bool mVisited = false;

        public NopJmp(int primaryJump, int secondaryJump, int position)
        {
            mPrimaryJump = primaryJump;
            mSecondaryJump = secondaryJump;
            mPosition = position;
        }

        public int ExecuteRecursively(IInstruction[] instructions, ref int acc, bool changeMade)
        {
            if(!mVisited)
            {
                mVisited = true;
                var primaryMove = mPosition + mPrimaryJump;
                var secondaryMove = mPosition + mSecondaryJump;

                if(primaryMove == instructions.Length
                    || secondaryMove == instructions.Length)
                {
                    return 1;
                }

                if(!changeMade)
                {
                    if(instructions[secondaryMove].ExecuteRecursively(instructions, ref acc, true) == 1)
                        return 1;
                }
                if(instructions[primaryMove].ExecuteRecursively(instructions, ref acc, changeMade) == 1)
                    return 1;
            }
            mVisited = false;
            return -1;
        }
    }
}