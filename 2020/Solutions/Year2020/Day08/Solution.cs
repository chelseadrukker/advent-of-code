namespace AdventOfCode.Solutions.Year2020
{
    class Day08 : ASolution
    {
        public Day08() : base(8, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            return new GameBoy1.GameBoy(Input).Solve();
        }

        protected override string SolvePartTwo()
        {
            return new GameBoy2.GameBoy(Input).Solve();
        }
    }
}
