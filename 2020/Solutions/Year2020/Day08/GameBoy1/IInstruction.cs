﻿namespace AdventOfCode.Solutions.Year2020.GameBoy1
{
    internal partial class GameBoy
    {
        public interface IInstruction
        {
            int Execute(ref int acc);
        }
    }
}