﻿namespace AdventOfCode.Solutions.Year2020.GameBoy1
{
    internal partial class GameBoy
    {
        internal class Jmp : IInstruction
        {
            private readonly int mValue;
            private readonly int mPosition;
            private bool mVisited = false;

            public Jmp(int value, int position)
            {
                mValue = value;
                mPosition = position;
            }

            public int Execute(ref int acc)
            {
                if(mVisited)
                {
                    return -1;
                }
                mVisited = true;
                return mPosition + mValue;
            }
        }
    }
}