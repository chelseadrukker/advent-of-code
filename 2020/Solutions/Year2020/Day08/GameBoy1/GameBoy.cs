﻿using System.Linq;

namespace AdventOfCode.Solutions.Year2020.GameBoy1
{
    internal partial class GameBoy
    {
        private readonly string mInput;

        public GameBoy(string input)
        {
            mInput = input;
        }

        internal string Solve()
        {
            var instructions = mInput
                .SplitByNewline()
                .Select((instruction, position) => new InstructionFactory(instruction, position).Create())
                .ToArray();

            var acc = 0;
            var pos = 0;
            while(pos >= 0)
            {
                pos = instructions[pos].Execute(ref acc);
            }
            return acc.ToString();
        }
    }
}