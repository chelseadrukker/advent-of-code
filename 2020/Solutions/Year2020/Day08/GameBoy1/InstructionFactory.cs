﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020.GameBoy1
{
    internal partial class GameBoy
    {
        internal class InstructionFactory
        {
            private string mInstruction;
            private readonly int mPosition;

            public InstructionFactory(string instruction, int position)
            {
                mInstruction = instruction;
                mPosition = position;
            }

            public IInstruction Create()
            {
                var match = Regex.Match(mInstruction, @"^(\w+) ((\+|-)\d+)$");
                var value = int.Parse(match.Groups[2].Value);
                var instructionType = match.Groups[1].Value;
                switch(instructionType)
                {
                    case "nop":
                        return new Jmp(1, mPosition);
                    case "acc":
                        return new Acc(value, mPosition);
                    case "jmp":
                        return new Jmp(value, mPosition);
                    default:
                        throw new Exception("Instruction type not recognized: {instructionType}");
                }
            }
        }
    }
}