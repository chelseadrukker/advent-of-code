using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day10 : ASolution {
        private const int ChargingOutlet = 0;
        private const int Adapter = 3;

        public Day10() : base(10, 2020, "") {

        }

        protected override string SolvePartOne() {
            var orderedAdapters = Input.SplitByNewline().Select(l => int.Parse(l)).Append(ChargingOutlet).OrderBy(_ => _).ToArray();
            var differences = orderedAdapters.Skip(1).Select((a, i) => a - orderedAdapters[i]).Append(Adapter);
            return (differences.Count(d => d == 1) * (differences.Count(d => d == 3))).ToString();
        }

        protected override string SolvePartTwo() {
            var tribonacciSequence = new long[] { 0, 1, 2, 4, 7, 13, 24, 44, 81, 149, 274, 504, 927, 1705 };

            var orderedAdapters = Input.SplitByNewline().Select(l => int.Parse(l)).Append(ChargingOutlet).OrderBy(_ => _).ToArray();
            var differences = orderedAdapters.Skip(1).Select((a, i) => a - orderedAdapters[i]).Append(Adapter);
            var groups = string.Join(string.Empty, differences).Split("3", StringSplitOptions.RemoveEmptyEntries);

            var tribonacci = groups.Select(g => tribonacciSequence[g.Length]);

            return tribonacci.Aggregate((a, b) => a * b).ToString("");
        }
    }
}
