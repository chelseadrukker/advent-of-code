﻿using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class EyeColor
    {
        public EyeColor(string v)
        {
            StringValue = v;
        }

        public string StringValue { get; }

        internal bool IsValid()
        {
            return Regex.IsMatch(StringValue, @"^(amb|blu|brn|gry|grn|hzl|oth)$");
        }
    }
}