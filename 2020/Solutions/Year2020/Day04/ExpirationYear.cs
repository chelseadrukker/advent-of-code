﻿namespace AdventOfCode.Solutions.Year2020
{
    internal class ExpirationYear
    {
        public string StringValue;

        public ExpirationYear(string v)
        {
            this.StringValue = v;
        }

        internal bool IsValid()
        {
            // explicitly check for length, because int can parse 4 digit number with leading zeros
            return StringValue.Length == 4
                && int.TryParse(StringValue, out int z)
                && z >= 2020
                && z <= 2030;
        }
    }
}