﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class HairColor
    {
        public HairColor(string v)
        {
            StringValue = v;
        }

        public string StringValue { get; }

        internal bool IsValid()
        {
            return Regex.IsMatch(StringValue, @"^#[0-9a-f]{6}$");
        }
    }
}