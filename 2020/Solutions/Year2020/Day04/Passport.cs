﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Passport
    {
        public override string ToString()
        {
            return $"BirthYear: {this.BirthYear.StringValue}{Environment.NewLine}" +
                $"IssueYear: {this.IssueYear.StringValue}{Environment.NewLine}" +
                $"ExpirationYear: {this.ExpirationYear.StringValue}{Environment.NewLine}" +
                $"Height: {this.Height.StringValue}{Environment.NewLine}" +
                $"HairColor: {this.HairColor.StringValue}{Environment.NewLine}" +
                $"EyeColor: {this.EyeColor.StringValue}{Environment.NewLine}" +
                $"PassportID: {this.PassportID.StringValue}{Environment.NewLine}" +
                $"CountryID: {this.CountryID.StringValue}{Environment.NewLine}";
        }

        public Passport(string rawPassport)
        {
            var matches = Regex
                .Matches(rawPassport, @"(\w+):([^\s]+)", RegexOptions.Multiline)
                .ToDictionary(m => m.Groups[1].ToString(), m => m.Groups[2].ToString());
            this.BirthYear = new BirthYear(ValueOrEmptyString(matches, "byr"));
            this.IssueYear = new IssueYear(ValueOrEmptyString(matches, "iyr"));
            this.ExpirationYear = new ExpirationYear(ValueOrEmptyString(matches, "eyr"));
            this.Height = new Height(ValueOrEmptyString(matches, "hgt"));
            this.HairColor = new HairColor(ValueOrEmptyString(matches, "hcl"));
            this.EyeColor = new EyeColor(ValueOrEmptyString(matches, "ecl"));
            this.PassportID = new PassportID(ValueOrEmptyString(matches, "pid"));
            this.CountryID = new CountryID(ValueOrEmptyString(matches, "cid"));
        }

        private static string ValueOrEmptyString(Dictionary<string, string> matches, string key)
        {
            return matches.ContainsKey(key) ? matches[key] : String.Empty;
        }

        public bool IsValid() {
            return BirthYear.IsValid()
                && IssueYear.IsValid()
                && ExpirationYear.IsValid()
                && Height.IsValid()
                && HairColor.IsValid()
                && EyeColor.IsValid()
                && PassportID.IsValid()
                && CountryID.IsValid();
        }

        public BirthYear BirthYear { get; }
        public IssueYear IssueYear { get; }
        public ExpirationYear ExpirationYear { get; }
        public Height Height { get; }
        public HairColor HairColor { get; }
        public EyeColor EyeColor { get; }
        public PassportID PassportID { get; }
        public CountryID CountryID { get; }
    }
}
