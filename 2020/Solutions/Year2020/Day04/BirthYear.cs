﻿using System;

namespace AdventOfCode.Solutions.Year2020
{
    internal class BirthYear
    {
        public string StringValue;

        public BirthYear(string v)
        {
            this.StringValue = v;
        }

        internal bool IsValid()
        {
            // explicitly check for length, because int can parse 4 digit number with leading zeros
            return StringValue.Length == 4
                && int.TryParse(StringValue, out int x)
                && x >= 1920
                && x <= 2002;
        }
    }
}