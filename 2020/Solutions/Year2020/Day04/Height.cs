﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Height
    {
        public string StringValue;

        public int Value { get; }
        public string Unit { get; }

        public Height(string v)
        {
            this.StringValue = v;
            Match match = Regex.Match(StringValue, @"^(\d+)(in|cm)$");
            int.TryParse(match.Groups[1].Value, out int value);
            this.Value = value;
            this.Unit = match.Groups[2].Value;
        }

        internal bool IsValid()
        {
            return (IsMetric() && Value >= 150 && Value <= 193)
                || (IsImperial() && Value >= 59 && Value <= 76);
        }

        private bool IsMetric()
        {
            return Unit == "cm";
        }

        private bool IsImperial()
        {
            return Unit == "in";
        }
    }
}