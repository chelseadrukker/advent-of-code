using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{

    class Day04 : ASolution {

        public Day04() : base(4, 2020, "") {

        }

        protected override string SolvePartOne() {
            var passports = Input.Split("\n\n").Select(p => ExtractPassportKeys(p));
            return passports.Count(p => p.Length == 8 || (p.Length == 7 && !p.Any(k => k == "cid"))).ToString();
        }

        private string[] ExtractPassportKeys(string p)
        {
            return Regex
                .Matches(p, @"(\w+):[^\s]+", RegexOptions.Multiline)
                .Select(m => m.Groups[1].ToString())
                .ToArray();
        }

        protected override string SolvePartTwo() {
            var passports = Input.Split("\n\n")
                .Select(p => new Passport(p));
            return passports.Count(p => p.IsValid()).ToString();
        }
    }
}
