﻿using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class PassportID
    {
        public PassportID(string v)
        {
            StringValue = v;
        }

        public string StringValue { get; }

        internal bool IsValid()
        {
            return Regex.IsMatch(StringValue, @"^\d{9}$");
        }
    }
}