﻿using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    public class CountryID
    {
        public CountryID(string v)
        {
            StringValue = v;
        }

        public string StringValue { get; }

        internal bool IsValid()
        {
            return true;
        }
    }
}