using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    public enum Side {
        Top,
        Right,
        Bottom,
        Left
    }

    class Day20 : ASolution
    {
        protected override string SolvePartOne()
        {
            var split = Input.Split("\n\n");
            var tilesWithDuplicates = split.SelectMany(t => new[] { new Tile(t), new Tile(t, true) });
            var allSides = tilesWithDuplicates.SelectMany(t => t.Sides);
            // tiles on the edges have only 2 sides whitch have neighbours
            var result = tilesWithDuplicates.Where(t => t.Sides.Count(s => allSides.Count(a => a == s) == 2) == 2);
            return result.Select(t => t.Id).Distinct().Aggregate((a, b) => a * b).ToString();
        }

        protected override string SolvePartTwo()
        {
            var split = Input.Split("\n\n");
            var tilesWithDuplicates = split.SelectMany(t => new[] { new Tile(t), new Tile(t, true) });
            //var tiles = LeaveTilesOnSameSide(tilesWithDuplicates);
            var tiles = tilesWithDuplicates.ToArray();
            var origin = FindAndPrepareFirstCorner(tiles);

            while(tiles.Count(t => t.X != null) < (tiles.Count() / 2))
            {
                origin = FindNeighboursInOneDirection(tiles, origin, Side.Right, Side.Left, 1, 0);
                origin = FindNeighboursInOneDirection(tiles, origin, Side.Bottom, Side.Top, 0, 1);
                origin = FindNeighboursInOneDirection(tiles, origin, Side.Left, Side.Right, -1, 0);
                origin = FindNeighboursInOneDirection(tiles, origin, Side.Top, Side.Bottom, 0, -1);
            }

            var map = new Map(tiles.Where(t => t.X != null));
            return map.Roughness.ToString();

            //return Utilities.ToJson(tiles.Where(t => t.X != null).OrderBy(t => t.X).ThenBy(t => t.Y).Select(t => t.Id));
        }

        private static Tile FindNeighboursInOneDirection(Tile[] tiles, Tile origin, Side direction, Side sideToMatch, int offsetX, int offsetY)
        {
            var reversedOriginSide = origin.GetSide(direction).Reverse().JoinAsStrings();
            var indicesTaken = tiles.Where(t => t.X != null).Select(t => t.Id);
            var next = tiles.Where(t => !indicesTaken.Any(i => i == t.Id))
                .FirstOrDefault(t => t.Sides.Any(s => s == reversedOriginSide));

            while(next != null)
            {
                Console.WriteLine(Enum.GetName(typeof(Side), direction));
                next.RotateToMatchSide(reversedOriginSide, sideToMatch);
                next.SetPosition(origin.X.Value + offsetX, origin.Y.Value + offsetY);
                origin = next;

                reversedOriginSide = origin.GetSide(direction).Reverse().JoinAsStrings();
                indicesTaken = tiles.Where(t => t.X != null).Select(t => t.Id);
                next = tiles.Where(t => !indicesTaken.Any(i => i == t.Id))
                    .FirstOrDefault(t => t.Sides.Any(s => s == reversedOriginSide));
            }
            return origin;
        }

        private static Tile FindAndPrepareFirstCorner(Tile[] tiles)
        {
            var allSides = tiles.SelectMany(t => t.Sides);
            var origin = tiles.First(t => t.Sides.Count(s => allSides.Count(a => a == s) == 2) == 2);
            var allSidesWithoutOrigin = tiles.Where(t => t.Id != origin.Id).SelectMany(t => t.Sides);
            origin.RotateToBeTopLeftOrigin(allSidesWithoutOrigin);
            origin.SetPosition(0, 0);
            return origin;
        }

        //private Tile[] LeaveTilesOnSameSide(IEnumerable<Tile> tilesWithDuplicates)
        //{
        //    var result = new List<Tile>() { tilesWithDuplicates.First() };
        //    while(result.Count() < tilesWithDuplicates.Count() / 2) {
        //        var resultSides = result.SelectMany(t => t.Sides);
        //        var tilesToAdd = tilesWithDuplicates
        //            .Where(t => t.Sides.Intersect(resultSides).Any())
        //            .Where(t => !result.Any(r => r.Id == t.Id));
        //        result.AddRange(tilesToAdd);
        //    }
        //    return result.ToArray();
        //}

        public Day20() : base(20, 2020, "")
        {
        }
    }

    internal static class Test
    {
        internal static string Input = @"Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...".Replace("\r", "");
    }
}