﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Tile
    {
        public Tile(string tile, bool flipContent = false)
        {
            var split = tile.SplitByNewline();
            Id = long.Parse(Regex.Match(split.First(), @"^Tile\s(\d+):$").Groups[1].Value);
            var content = split.Skip(1);
            Content = flipContent ? content.FlipOnX() : content;
        }

        public void SetPosition(int x, int y) {
            X = x;
            Y = y;
            Console.WriteLine(Id + " " + X + " " + Y);
        }

        public long Id { get; }
        public string Top => Content.First().JoinAsStrings();
        public string Right => Content.Select(l => l.Last()).JoinAsStrings();
        public string Bottom => Content.Last().Reverse().JoinAsStrings();
        public string Left => Content.Reverse().Select(l => l.First()).JoinAsStrings();
        public IEnumerable<IEnumerable<char>> Content { get; private set; }
        public IEnumerable<IEnumerable<char>> ImagePart => Content.Skip(1).Select(l => l.Skip(1).SkipLast(1)).SkipLast(1);
        public string[] Sides => new string[] { Top, Right, Bottom, Left };
        public string GetSide(Side side) {
            switch(side)
            {
                case Side.Top:
                    return Top;
                case Side.Right:
                    return Right;
                case Side.Bottom:
                    return Bottom;
                case Side.Left:
                    return Left;
                default:
                    throw new Exception($"Unknown side! {side}");
            }
        }
        public int? X { get; private set; }
        public int? Y { get; private set; }

        internal void RotateToBeTopLeftOrigin(IEnumerable<string> allSides)
        {
            var matchedSides = allSides.Intersect(Sides);
            while(!(matchedSides.Contains(Right) && matchedSides.Contains(Bottom)))
                Content = Content.Rotate();
        }

        internal void RotateToMatchSide(string side, Side sideToBeMatched)
        {
            while(side != GetSide(sideToBeMatched))
                Content = Content.Rotate();
        }
    }
}