﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Map
    {
        //LOOKAHED!!!! Monsters can overlap...
        private const string Monster = @"(?=((..................#.).{{{0}}}(#....##....##....###).{{{0}}}(.#..#..#..#..#..#...)))";
        public Map(IEnumerable<Tile> tiles)
        {
             Image = Print(tiles);
        }

        public int Roughness => Image.SelectMany(r => r).Count(c => c == '#') - (Monster.Count(c => c == '#') * MonsterCount());

        public int MonsterCount() {
            var monsterRegex = string.Format(Monster, Image.First().Count() - 20);
            var counts = new List<int>();
            Debugger.Launch();
            var temp = Image;
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Transpose();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            temp = temp.Rotate();
            counts.Add(Regex.Matches(temp.SelectMany(r => r).JoinAsStrings(), monsterRegex).Count());

            return counts.Max();
        }

        public IEnumerable<IEnumerable<char>> Image { get; }

        internal IEnumerable<IEnumerable<char>> Print(IEnumerable<Tile> tiles)
        {
            var rows = new List<List<char>>();
            for(int y = 0; y <= tiles.Select(t => t.Y).Max(); y++)
            {
                rows.AddRange(tiles
                    .Where(t => t.Y == y)
                    .OrderBy(t => t.X)
                    .Select(t => t.ImagePart)
                    .Aggregate((a, b) => a.Zip(b, (i, j) => i.Concat(j)))
                    .Select(r => r.ToList()));
            }
            return rows;
        }
    }
}