using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day05 : ASolution {

        public Day05() : base(5, 2020, "") {

        }

        protected override string SolvePartOne() {
            var boardingPasses = Input.SplitByNewline().Select(p => ID(p));
            return boardingPasses.Max().ToString();
        }

        private int ID(string p)
        {
            var row = Convert.ToInt32(p.Substring(0,7).Replace("F","0").Replace("B", "1"), 2);
            var col = Convert.ToInt32(p.Substring(7).Replace("L","0").Replace("R", "1"), 2);
            return row * 8 + col;
        }

        protected override string SolvePartTwo() {
            var boardingPasses = Input.SplitByNewline().Select(p => ID(p));
            return Enumerable
                .Range(boardingPasses.Min(), boardingPasses.Count() + 1)
                .Except(boardingPasses)
                .Single()
                .ToString();
        }
    }
}
