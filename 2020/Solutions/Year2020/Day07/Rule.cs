﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Rule
    {
        public string BagColor { get; }
        public IEnumerable<BagGroup> Content { get; }
        public Rule(string rule)
        {
            var split = rule
                .Split(new []{ "bags contain", "no other bags", ",", "." }, System.StringSplitOptions.TrimEntries)
                .Where(r => !string.IsNullOrWhiteSpace(r));
            BagColor = split.First();
            Content = split
                .Skip(1)
                .Select(bg => new BagGroup(bg));
        }
        public override string ToString()
        {
            return $"{BagColor}: {string.Join(", ", Content.Select(c => c.Size + " " + c.Color))}";
        }
    }
}
