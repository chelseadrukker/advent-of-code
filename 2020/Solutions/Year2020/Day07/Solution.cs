using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day07 : ASolution {

        private readonly IEnumerable<Rule> Rules;

        public Day07() : base(7, 2020, "") {
            Rules = Input
                .SplitByNewline()
                .Select(rule => new Rule(rule));
        }

        protected override string SolvePartOne() {
            var results = new HashSet<string>();
            var currentLevel = new HashSet<string> { "shiny gold" };

            while (currentLevel.Any()) {
                currentLevel = Rules
                    .Where(rule => rule.Content.Any(bucket => currentLevel.Contains(bucket.Color)))
                    .Select(rule => rule.BagColor)
                    .ToHashSet();
                results = results.Concat(currentLevel).ToHashSet();
            }
            return results.Count().ToString();
        }

        protected override string SolvePartTwo() {
            var result = GetTotalContentRecursive("shiny gold") - 1;
            return result.ToString();
        }

        private int GetTotalContentRecursive(string color)
        {
            return Rules
                .Single(r => r.BagColor == color)
                .Content
                .Select(bucket => GetTotalContentRecursive(bucket.Color) * bucket.Size)
                .Sum()
                + 1;
        }
    }

}
