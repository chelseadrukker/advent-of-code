﻿using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class BagGroup
    {

        public BagGroup(string group)
        {
            string[] split = group.Split(" ");
            Size = int.Parse(split.First());
            Color = string.Join(' ', split.Skip(1).Take(2));
        }

        public int Size { get; }
        public string Color { get; }
    }
}