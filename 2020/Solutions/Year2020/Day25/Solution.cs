using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    class Day25 : ASolution
    {
        protected override string SolvePartOne()
        {
            var publicKeys = Input.SplitByNewline().Select(pk => int.Parse(pk));
            int subjectNumber = 7;
            int remainder = 0;
            BigInteger value = 1;
            long divisor = 20201227;
            var loop = 0;
            while(!publicKeys.Contains(remainder)) {
                value *= subjectNumber;
                remainder = (int)(value % divisor);
                loop++;
            }

            BigInteger newSubject = publicKeys.First(pk => pk != remainder);
            return Utilities.ToJson(BigInteger.ModPow(newSubject, loop, divisor));
        }

        protected override string SolvePartTwo()
        {
            var result = Test25.Input.SplitByNewline();
            return Utilities.ToJson(result);
        }

        public Day25() : base(25, 2020, "")
        {
        }
    }

    internal static class Test25
    {
        internal static string Input = @"5764801
17807724".Replace("\r", string.Empty);
    }
}