﻿using System;
using System.Drawing;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Ferry2
    {
        public Ferry2()
        {
            mLocation = new Point(0, 0);
            mWaypoint = new Waypoint(10, -1);
        }

        private Point mLocation;

        private Waypoint mWaypoint;

        public string Distance => (Math.Abs(mLocation.X) + Math.Abs(mLocation.Y)).ToString();

        internal void Move(string instruction)
        {
            var command = instruction[0];
            var value = int.Parse(instruction.Substring(1));
            switch(command)
            {
                case 'N':
                    mWaypoint.Move(0, -value);
                    break;
                case 'W':
                    mWaypoint.Move(-value, 0);
                    break;
                case 'S':
                    mWaypoint.Move(0, value);
                    break;
                case 'E':
                    mWaypoint.Move(value, 0);
                    break;
                case 'R':
                    mWaypoint.RotateRight(value/90);
                    Console.WriteLine($"Rotated {value} right to {mWaypoint.Vector().X} x {mWaypoint.Vector().Y}");
                    break;
                case 'L':
                    mWaypoint.RotateLeft(value/90);
                    Console.WriteLine($"Rotated {value} left to {mWaypoint.Vector().X} x {mWaypoint.Vector().Y}");
                    break;
                case 'F':
                    var vector = mWaypoint.Vector();
                    mLocation.X += vector.X * value;
                    mLocation.Y += vector.Y * value;
                    Console.WriteLine($"X:{mLocation.X} Y:{mLocation.Y}");
                    break;
                default:
                    throw new Exception($"Invalid direction: ({command})");
            }
        }

        private class Waypoint
        {
            public Waypoint(int x, int y)
            {
                mX = x;
                mY = y;
            }

            private int mX;
            private int mY;


            internal void RotateLeft(int times)
            {
                for(int i = 0; i < times; i++)
                {
                    var x = mX;
                    mX = mY;
                    mY = -x;
                }
            }

            internal void RotateRight(int times)
            {
                for(int i = 0; i < times; i++)
                {
                    var x = mX;
                    mX = -mY;
                    mY = x;
                }
            }

            internal void Move(int x, int y)
            {
                mX += x;
                mY += y;
            }

            public Point Vector()
            {
                return new Point(mX, mY);
            }
        }
    }
}