namespace AdventOfCode.Solutions.Year2020 {

    class Day12 : ASolution {

        public Day12() : base(12, 2020, "") {

        }

        protected override string SolvePartOne() {
            var ferry = new Ferry1();
            var instructions = Input.SplitByNewline();
            foreach(var instruction in instructions)
                ferry.Move(instruction);
            return ferry.Distance;
        }

        protected override string SolvePartTwo()
        {
            var ferry = new Ferry2();
            var instructions = Input.SplitByNewline();
            foreach(var instruction in instructions)
                ferry.Move(instruction);
            return ferry.Distance;
        }
    }
}
