﻿using System;
using System.Drawing;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Ferry1
    {
        public Ferry1()
        {
            mLocation = new Point(0, 0);
            mDirection = new Direction(0);
        }

        private Point mLocation;

        private Direction mDirection;

        public string Distance => (Math.Abs(mLocation.X) + Math.Abs(mLocation.Y)).ToString();

        internal void Move(string instruction)
        {
            var direction = instruction[0];
            var value = int.Parse(instruction.Substring(1));
            switch(direction)
            {
                case 'N':
                    mDirection.Set(270);
                    break;
                case 'W':
                    mDirection.Set(180);
                    break;
                case 'S':
                    mDirection.Set(90);
                    break;
                case 'E':
                    mDirection.Set(0);
                    break;
                case 'F':
                    break;
                case 'R':
                    mDirection.RotateRight(value);
                    Console.WriteLine($"Rotated {value} right to {mDirection.Degree}");
                    return;
                case 'L':
                    mDirection.RotateLeft(value);
                    Console.WriteLine($"Rotated {value} left to {mDirection.Degree}");
                    return;
                default:
                    throw new Exception($"Invalid direction: ({direction})");
            }
            var vector = mDirection.Vector();
            mLocation.X += vector.X * value;
            mLocation.Y += vector.Y * value;
            Console.WriteLine($"X:{mLocation.X} Y:{mLocation.Y}");
            mDirection.Reset();
        }

        private class Direction
        {
            public Direction(int degree)
            {
                Degree = degree;
            }

            private int mBackup = -1;

            public int Degree { get; private set; }

            internal void RotateLeft(int value)
            {
                var tmp = Degree - value;
                Degree = tmp < 0
                    ? tmp + 360
                    : tmp % 360;
            }

            internal void RotateRight(int value)
            {
                Degree = (Degree + value) % 360;
            }

            internal void Set(int degree)
            {
                mBackup = Degree;
                Degree = degree;
            }

            public Point Vector()
            {
                switch(Degree)
                {
                    case 0:
                        return new Point(1, 0);
                    case 90:
                        return new Point(0, 1);
                    case 180:
                        return new Point(-1, 0);
                    case 270:
                        return new Point(0, -1);
                    default:
                        throw new Exception($"Invalid degree: ({Degree})");
                }
            }

            internal void Reset()
            {
                if(mBackup != -1)
                {
                    Degree = mBackup;
                    mBackup = -1;
                }
            }
        }
    }
}