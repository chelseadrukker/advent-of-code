using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020 {

    class Day02 : ASolution {

        public Day02() : base(2, 2020, "") {

        }

        protected override string SolvePartOne() {
            var passwords = Input.SplitByNewline().Select(i => new Password(i));
            return passwords.Select(p => p.Validate()).Sum().ToString();
        }

        protected override string SolvePartTwo() {
            var passwords = Input.SplitByNewline().Select(i => new Password(i));
            return passwords.Select(p => p.TobogganValidate()).Sum().ToString();
        }
    }

    internal class Password
    {
        private readonly char Character;
        private readonly string PasswordText;
        private readonly int Min;
        private readonly int Max;

        public Password(string input)
        {
            var match = Regex.Match(input, @"(?<min>\d+)-(?<max>\d+)\s(?<character>[a-zA-Z]):\s(?<passwordText>\w+)");
            this.Min = int.Parse(match.Groups["min"].Value);
            this.Max = int.Parse(match.Groups["max"].Value);
            this.Character = match.Groups["character"].Value[0];
            this.PasswordText = match.Groups["passwordText"].Value;
        }

        public int Validate() {
            int cnt = PasswordText.Count(c => c == Character);
            return Min <= cnt && cnt <= Max ? 1 : 0;
        }

        public int TobogganValidate() {
            return PasswordText[Min - 1] == Character ^ PasswordText[Max - 1] == Character ? 1 : 0;
        }
    }
}
