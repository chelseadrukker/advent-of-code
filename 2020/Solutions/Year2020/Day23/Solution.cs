using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    class Day23 : ASolution
    {
        protected override string SolvePartOne()
        {
            var cups = Input.ToCharArray().Select(c => c - '0').ToList();
            var piecesToMove = 3;
            for(int i = 0; i < 100; i++)
            {
                var cupsToMove = cups.Skip(1).Take(piecesToMove);
                var current = cups.ElementAt(0);
                var target = GetTarget(current, cupsToMove);
                var targetPosition = cups.IndexOf(target);
                var firstPart = cups
                    .Skip(1 + piecesToMove)
                    .Take(targetPosition - (piecesToMove));
                var newCups = firstPart
                    .Concat(cupsToMove)
                    .Concat(cups.Skip(targetPosition + 1))
                    .Concat(new[] { current });
                cups = newCups.ToList();
            }
            return Utilities.ToJson(cups.JoinAsStrings().Split('1').Reverse().JoinAsStrings());

            static int GetTarget(int current, IEnumerable<int> cupsToMove)
            {
                var target = (10 + current - 1) % 10;
                while(cupsToMove.Contains(target) || target == 0)
                    target = (10 + target - 1) % 10;
                return target;
            }
        }

        protected override string SolvePartTwo()
        {
            var range = 1000000;
            var iterations = 10000000;
            var piecesToMove = 3;

            var ints = Input.ToCharArray().Select(c => c - '0').Concat(Enumerable.Range(10, range - 9));
            var cups = new LinkedList<int>();
            var index = new Dictionary<int, LinkedListNode<int>>();

            foreach(var i in ints) {
                var node = cups.AddLast(i);
                index[i] = node;
            }

            for(int i = 0; i < iterations; i++)
            {
                var current = cups.First;
                var cupsToMove = new List<LinkedListNode<int>>();
                for(int j = 0; j < piecesToMove; j++)
                {
                    var next = current.Next ?? cups.First;
                    cupsToMove.Add(next);
                    next = next.Next;
                    cups.Remove(next.Previous);
                }
                cups.Remove(current);

                var target = index[GetTarget(current.Value, cupsToMove, range)];

                foreach(var cup in cupsToMove)
                {
                    cups.AddAfter(target, cup);
                    target = cup;
                }

                cups.AddLast(current);
            }
            var zeroIndex = cups.ToList().IndexOf(1);
            return (cups.ElementAt(zeroIndex + 1) * cups.ElementAt(zeroIndex + 2)).ToString();

            static int GetTarget(int current, List<LinkedListNode<int>> cupsToMove, int numberOfCups)
            {
                var negativeGuard = numberOfCups + 1;
                var target = (negativeGuard + current - 1) % negativeGuard;
                while(cupsToMove.Any(c => c.Value == target) || target == 0)
                    target = (negativeGuard + target - 1) % negativeGuard;
                return target;
            }
        }

        public Day23() : base(23, 2020, "")
        {
        }
    }

    internal static class Test23
    {
        internal static string Input = @"389125467".Replace("\r", string.Empty);
    }
}