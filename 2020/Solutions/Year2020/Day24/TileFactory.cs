﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{
    internal static class TileFactory
    {
        public static HexTile Create(string row)
        {
            var matches = Regex.Matches(row, "w|nw|ne|e|se|sw").Select(m => m.Value);
            int x = 0, y = 0;
            foreach(var match in matches)
            {
                switch(match)
                {
                    case "w":
                        x--;
                        continue;
                    case "nw":
                        y--;
                        continue;
                    case "ne":
                        x++;
                        y--;
                        continue;
                    case "e":
                        x++;
                        continue;
                    case "se":
                        y++;
                        continue;
                    case "sw":
                        x--;
                        y++;
                        continue;
                    default:
                        throw new Exception($"Unknown direction! {match}");
                }
            }
            return new HexTile(x, y);
        }
    }
}