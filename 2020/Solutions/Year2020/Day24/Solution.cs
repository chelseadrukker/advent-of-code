using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    class Day24 : ASolution
    {
        protected override string SolvePartOne()
        {
            var tiles = Input.SplitByNewline()
                .Select(r => TileFactory.Create(r));
            var result = tiles.GroupBy(t => t.Key)
                .Count(g => g.Count() % 2 == 1);
            return Utilities.ToJson(result);
        }

        protected override string SolvePartTwo()
        {
            var lobby = new Lobby(Input.SplitByNewline());
            for(int i = 0; i < 100; i++)
            {
                lobby.Tick();
            }
            return Utilities.ToJson(lobby.BlackTilesCount());
        }

        public Day24() : base(24, 2020, "")
        {
        }
    }

    internal static class Test24
    {
        internal static string Input = @"sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew".Replace("\r", string.Empty);
    }
}