﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class HexTile
    {
        public int X { get; }
        public int Y { get; }
        public string Key { get; }

        public HexTile(int x, int y)
        {
            X = x;
            Y = y;
            Key = $"{x}x{y}";
        }

        public List<HexTile> GetNeighbours()
        {
            return new[] {
                new HexTile(X, Y - 1),
                new HexTile(X + 1, Y - 1),
                new HexTile(X - 1, Y),
                new HexTile(X + 1, Y),
                new HexTile(X - 1, Y + 1),
                new HexTile(X, Y + 1),
            }.ToList();
        }
    }
}