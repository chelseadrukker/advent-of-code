﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    internal class Lobby
    {
        private IEnumerable<HexTile> mBlackTiles;

        public Lobby(string[] moves)
        {
            mBlackTiles = moves
            .Select(r => TileFactory.Create(r))
            .GroupBy(t => t.Key)
            .Where(g => g.Count() % 2 == 1)
            .Select(g => g.First());
        }

        public void Tick()
        {
            var newBlackTiles = new List<HexTile>();
            var tilesToCheck = mBlackTiles
                .SelectMany(t => t.GetNeighbours())
                .Concat(mBlackTiles)
                .GroupBy(t => t.Key)
                .Select(g => g.First());
            var blackKeys = mBlackTiles.Select(t => t.Key).ToHashSet();
            foreach(var tile in tilesToCheck)
            {
                var isBlack = blackKeys.Contains(tile.Key);
                if(isBlack)
                {
                    var blackNeighboursCount = tile.GetNeighbours().Where(n => blackKeys.Contains(n.Key)).Count();
                    var staysBlack = blackNeighboursCount > 0 && blackNeighboursCount < 3;
                    if(staysBlack) newBlackTiles.Add(tile);
                } else
                {
                    var getsBlack = tile.GetNeighbours().Where(n => blackKeys.Contains(n.Key)).Count() == 2;
                    if(getsBlack) newBlackTiles.Add(tile);
                }
            }
            mBlackTiles = newBlackTiles;
        }

        internal object BlackTilesCount()
        {
            return mBlackTiles.Count();
        }
    }
}