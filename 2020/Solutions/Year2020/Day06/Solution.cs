using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020 {

    class Day06 : ASolution {

        public Day06() : base(6, 2020, "") {

        }

        protected override string SolvePartOne() {
            var groups = Input.Split("\n\n");
            return groups.Select(g => g.Distinct().Except("\n").Count()).Sum().ToString();
        }

        protected override string SolvePartTwo() {
            var groups = Input.Split("\n\n");
            return groups.Select(g => AllYes(g)).Sum().ToString();
        }

        private int AllYes(string groupAnswers)
        {
            var groupSize = groupAnswers.Split("\n").Length;
            return groupAnswers
                .GroupBy(c => c)
                .Where(g => g.Count() == groupSize && g.Key >= 'a' && g.Key <= 'z')
                .Count();
        }
    }
}
