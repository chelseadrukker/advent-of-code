﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Solutions.Year2019
{
    internal class OpcodeFactory
    {
        public IOpcode Create(string instruction, Stack<int> inputs, StringBuilder builder)
        {
            instruction = instruction.PadLeft(5, '0');
            string operation = instruction.Substring(3, 2);
            switch (operation)
            {
                case "01":
                    return new Add(instruction);
                case "02":
                    return new Multiply(instruction);
                case "03":
                    return new Set(inputs);
                case "04":
                    return new Print(instruction, builder);
                case "05":
                    return new JumpIf(instruction, false);
                case "06":
                    return new JumpIf(instruction, true);
                case "07":
                    return new LessThan(instruction);
                case "08":
                    return new Equals(instruction);
                default:
                    return new Halt();
            }
        }
    }

    internal class Halt : IOpcode
    {
        int IOpcode.Execute(int[] memory, int position)
        {
            return -1;
        }
    }

    internal class Print : IOpcode
    {
        private readonly string mInstruction;
        private readonly StringBuilder mBilder;

        public Print(string instruction, StringBuilder builder)
        {
            mInstruction = instruction;
            mBilder = builder;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var val = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            mBilder.Append($"{val} ");
            return position + 2;
        }
    }

    internal class Set : IOpcode
    {
        private readonly Stack<int> mInputs;

        public Set(Stack<int> inputs)
        {
            mInputs = inputs;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            memory[memory[position + 1]] = mInputs.Count > 1 ? mInputs.Pop() : mInputs.Peek();
            return position + 2;
        }
    }

    internal class Multiply : IOpcode
    {
        private string mInstruction;

        public Multiply(string instruction)
        {
            mInstruction = instruction;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var param1 = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            var param2 = mInstruction[1] == '1' ? memory[position + 2] : memory[memory[position + 2]];
            var resultPosition = memory[position + 3];
            memory[resultPosition] = param1 * param2;
            return position + 4;
        }
    }

    internal class Add : IOpcode
    {
        private readonly string mInstruction;

        public Add(string instruction)
        {
            mInstruction = instruction;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var param1 = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            var param2 = mInstruction[1] == '1' ? memory[position + 2] : memory[memory[position + 2]];
            var resultPosition = memory[position + 3];
            memory[resultPosition] = param1 + param2;
            return position + 4;
        }
    }

    internal class JumpIf : IOpcode
    {
        private readonly string mInstruction;
        private readonly bool mValue;

        public JumpIf(string instruction, bool value)
        {
            mInstruction = instruction;
            mValue = value;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var param1 = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            var param2 = mInstruction[1] == '1' ? memory[position + 2] : memory[memory[position + 2]];
            return (param1 == 0) == mValue ? param2 : position + 3;
        }
    }

    internal class LessThan : IOpcode
    {
        private readonly string mInstruction;

        public LessThan(string instruction)
        {
            mInstruction = instruction;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var param1 = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            var param2 = mInstruction[1] == '1' ? memory[position + 2] : memory[memory[position + 2]];
            var resultPosition = memory[position + 3];
            memory[resultPosition] = param1 < param2 ? 1 : 0;
            return position + 4;
        }
    }

    internal class Equals : IOpcode
    {
        private readonly string mInstruction;

        public Equals(string instruction)
        {
            mInstruction = instruction;
        }

        int IOpcode.Execute(int[] memory, int position)
        {
            var param1 = mInstruction[2] == '1' ? memory[position + 1] : memory[memory[position + 1]];
            var param2 = mInstruction[1] == '1' ? memory[position + 2] : memory[memory[position + 2]];
            var resultPosition = memory[position + 3];
            memory[resultPosition] = param1 == param2 ? 1 : 0;
            return position + 4;
        }
    }

    public interface IOpcode
    {
        int Execute(int[] memory, int position);
    }
}
