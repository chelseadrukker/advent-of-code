using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode.Solutions.Year2019
{

    class Day05 : ASolution {

        public Day05() : base(5, 2019, "") {

        }

        protected override string SolvePartOne()
        {
            var code = GetInput();
            return ExecuteOperations(code, 1);
        }

        private int[] GetInput()
        {
            return Input.SplitByNewline()[0].Split(",").Select(i => int.Parse(i)).ToArray();
        }
        public string ExecuteOperations(int[] code, int input, int? noun = null, int? verb = null)
        {
            var inputs = new Stack<int>();
            inputs.Push(input);
            return ExecuteOperations(code, inputs, noun, verb);
        }

        public string ExecuteOperations(int[] code, Stack<int> input, int? noun = null, int? verb = null)
        {
            var memory = (int[])code.Clone();
            memory[1] = noun.HasValue ? noun.Value : memory[1];
            memory[2] = verb.HasValue ? verb.Value : memory[2];
            var index = 0;
            var builder = new StringBuilder();
            while (index >= 0)
            {
                index = new OpcodeFactory().Create(memory[index].ToString(), input, builder).Execute(memory, index);
            }
            return builder.ToString().Trim().Split(' ').Last();
        }

        protected override string SolvePartTwo()
        {
            var code = GetInput();
            return ExecuteOperations(code, 5);
        }
    }
}
