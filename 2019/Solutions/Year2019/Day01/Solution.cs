using System;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019 {

    class Day01 : ASolution {

        public Day01() : base(1, 2019, "") {

        }

        protected override string SolvePartOne()
        {
            return Solve(CalculateFuel);
        }

        protected override string SolvePartTwo()
        {
            return Solve(CalculateFuelUntilZero);
        }

        private string Solve(Func<int, int> CalculateFuel)
        {
            return Input.SplitByNewline().ToList().Select(mass => CalculateFuel(int.Parse(mass))).Sum().ToString();
        }

        private int CalculateFuel(int mass)
        {
            return Fuel(mass);
        }

        private int CalculateFuelUntilZero(int mass)
        {
            int result = Fuel(mass);
            bool zeroReached = result <= 0;
            return zeroReached ? 0 : result + CalculateFuelUntilZero(result);
        }

        private static int Fuel(int mass)
        {
            return (int)Math.Floor((double)mass / 3) - 2;
        }
    }
}
