using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019
{

    class Day04 : ASolution
    {
        public Day04() : base(4, 2019, "")
        {

        }

        protected override string SolvePartOne()
        {
            return FirstPart().Count().ToString();
        }

        private bool IncreasingWithRepeat(string number)
        {
            var pairs = number.Zip(number.Substring(1));
            return pairs.All(pair => char.GetNumericValue(pair.First) <= char.GetNumericValue(pair.Second))
                && pairs.Any(pair => pair.First == pair.Second);
        }

        private bool OneOrMoreDoubles(string number)
        {
            return number.GroupBy(d => d).Any(g => g.Count() == 2);
        }

        protected override string SolvePartTwo()
        {
            return FirstPart().Where(OneOrMoreDoubles).Count().ToString();
        }

        private IEnumerable<string> FirstPart()
        {
            var input = Input.Split("-");
            var start = int.Parse(input[0]);
            var end = int.Parse(input[1]);
            var result = Enumerable.Range(start, end - start)
                .Select(i => i.ToString())
                .Where(IncreasingWithRepeat);
            return result;
        }
    }
}
