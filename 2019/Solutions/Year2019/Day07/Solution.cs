using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019 {

    class Day07 : ASolution {

        public Day07() : base(7, 2019, "") {

        }

        protected override string SolvePartOne() {
            var code = GetInput();
            var intcode = new Day05();
            int? max = null;
            foreach (var permutaion in Permutations("01234".ToCharArray()))
            {
                var result = 0;
                foreach (var setting in permutaion.Select(c => int.Parse(c.ToString())))
                {
                    var inputs = new Stack<int>();
                    inputs.Push(result);
                    inputs.Push(setting);
                    result = int.Parse(intcode.ExecuteOperations(code, inputs));
                }
                max = max.HasValue ? (result > max ? result : max) : result;
            }
            return max.ToString();
        }

        private IEnumerable<IEnumerable<char>> Permutations(IEnumerable<char> source)
        {
            var c = source.Count();
            if (c == 1)
                yield return source;
            else
                for (int i = 0; i < c; i++)
                    foreach (var p in Permutations(source.Take(i).Concat(source.Skip(i + 1)).ToArray()))
                        yield return source.Skip(i).Take(1).Concat(p);
        }

        private int[] GetInput()
        {
            return Input.SplitByNewline()[0].Split(",").Select(i => int.Parse(i)).ToArray();
        }

        protected override string SolvePartTwo() {
            return null;
        }
    }
}
