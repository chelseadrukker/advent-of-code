using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019 {

    class Day06 : ASolution {

        public Day06() : base(6, 2019, "") {

        }

        protected override string SolvePartOne() {
            var neighbourhoods = Input.SplitByNewline().Select(o => new Neighbourhood(o.Split(")")));
            var center = "COM";
            return GetNeighbourCount(center, neighbourhoods, 0).ToString();
        }

        private int GetNeighbourCount(string center, IEnumerable<Neighbourhood> neighbours, int indirectNeighbours)
        {
            var count = indirectNeighbours;
            foreach (var neighbour in neighbours.Where(nb => nb.Center == center))
            {
                count += GetNeighbourCount(neighbour.Orbiting, neighbours, indirectNeighbours + 1);
            }
            return count;
        }

        protected override string SolvePartTwo() {
            var neighbourhoods = Input.SplitByNewline().Select(o => new Neighbourhood(o.Split(")")));
            var me = "YOU";
            var santa = "SAN";
            var myNeighbours = new List<string>();
            var santaNeighbours = new List<string>();
            while (myNeighbours.Intersect(santaNeighbours).Count() < 1) {
                me = neighbourhoods.Single(n => n.Orbiting == me).Center;
                myNeighbours.Add(me);
                santa = neighbourhoods.Single(n => n.Orbiting == santa).Center;
                santaNeighbours.Add(santa);
            }
            var joint = myNeighbours.Intersect(santaNeighbours).Single();
            var myPath = myNeighbours.TakeWhile(n => n != joint).Count();
            var santaPath = santaNeighbours.TakeWhile(n => n != joint).Count();
            return (myPath + santaPath).ToString();
        }
    }

    internal class Neighbourhood
    {
        public string Center { get; private set; }
        public string Orbiting { get; private set; }

        public Neighbourhood(string[] relation)
        {
            Center = relation[0];
            Orbiting = relation[1];
        }
    }
}
