using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019 {

    class Day02 : ASolution {

        public Day02() : base(2, 2019, "") {

        }

        protected override string SolvePartOne()
        {
            var input = GetInput();
            return ExecuteOperations(input, 12, 2)[0].ToString();
        }

        private int[] GetInput()
        {
            return Input.Split(",").Select(i => int.Parse(i)).ToArray();
        }

        protected override string SolvePartTwo() {
            var input = GetInput();
            foreach (var combination in Combinations()) {
                if (ExecuteOperations(input, combination.Noun, combination.Verb)[0] == 19690720)
                    return (100 * combination.Noun + combination.Verb).ToString();
            }
            return "unknown";
        }

        private IEnumerable<Input> Combinations()
        {
            var range = Enumerable.Range(0, 100);
            return range.SelectMany(r => range.Select(c => new Input() { Noun = r, Verb = c }));
        }

        private int[] ExecuteOperations(int[] input, int noun, int verb)
        {
            var memory = (int[])input.Clone();
            memory[1] = noun;
            memory[2] = verb;
            var index = 0;
            while (index >= 0)
            {
                index = ExecuteOperationAt(memory, index);
            }
            return memory;
        }

        private static int ExecuteOperationAt(int[] input, int position)
        {
            var opcode = input[position++];
            var inputPosition1 = input[position++];
            var inputPosition2 = input[position++];
            var resultPosition = input[position++];
            //TODO OperationFactory
            switch (opcode)
            {
                case 1:
                    input[resultPosition] = input[inputPosition1] + input[inputPosition2];
                    return position;
                case 2:
                    input[resultPosition] = input[inputPosition1] * input[inputPosition2];
                    return position;
                default:
                    return -1;
            }
        }
    }

    internal class Input
    {
        public int Noun { get; set; }
        public int Verb { get; set; }
    }
}
