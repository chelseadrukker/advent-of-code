using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AdventOfCode.Solutions.Year2019 {

    class Day03 : ASolution {

        public Day03() : base(3, 2019, "") {

        }

        protected override string SolvePartOne()
        {
            var input = GetInput();
            var wire1Path = GetPath(input[0]);
            var wire2Path = GetPath(input[1]);
            var intersections = wire1Path.Intersect(wire2Path);
            return intersections.Select(i => ManhattanDistance(i)).OrderBy(d => d).ElementAt(1).ToString();
        }

        private int ManhattanDistance(Point intersection)
        {
            return Math.Abs(intersection.X) + Math.Abs(intersection.Y);
        }

        private List<Point> GetPath(string[] wire)
        {
            var position = new Point() { X = 0, Y = 0 };
            var path = new List<Point>() { position };
            foreach (var instruction in wire) {
                int distance = int.Parse(instruction.Substring(1));
                char direction = instruction[0];
                switch (direction) {
                    case 'R':
                        path.AddRange(Enumerable.Range(position.X + 1, distance).Select(r => new Point(r, position.Y)));
                        position.X += distance;
                        break;
                    case 'D':
                        path.AddRange(Enumerable.Range(position.Y + 1, distance).Select(r => new Point(position.X, r)));
                        position.Y += distance;
                        break;
                    case 'L':
                        path.AddRange(Enumerable.Range(position.X - distance, distance).Select(r => new Point(r, position.Y)).Reverse());
                        position.X -= distance;
                        break;
                    case 'U':
                        path.AddRange(Enumerable.Range(position.Y - distance, distance).Select(r => new Point(position.X, r)).Reverse());
                        position.Y -= distance;
                        break;
                    default:
                        break;
                }
            }
            return path;
        }

        private string[][] GetInput()
        {
            return Input.SplitByNewline().Select(r => r.Split(",")).ToArray();
        }

        protected override string SolvePartTwo() {
            var input = GetInput();
            var wire1Path = GetPath(input[0]);
            var wire2Path = GetPath(input[1]);
            var intersections = wire1Path.Intersect(wire2Path);
            var distances = intersections.Select(i => wire1Path.IndexOf(i) + wire2Path.IndexOf(i));
            return distances.OrderBy(d => d).ElementAt(1).ToString();
        }
    }
}
