﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Level2
    {
        public List<List<int>> Spreadsheet { get; set; }

        public Level2(string input)
        {
            using (System.IO.StringReader reader = new System.IO.StringReader(input))
            {
                Spreadsheet = new List<List<int>>();
                string line;
                do
                {
                    line = reader.ReadLine();
                    if (line != null) Spreadsheet.Add(line.Split('\t').Select(Int32.Parse).ToList());
                } while (!string.IsNullOrEmpty(line));
            }
        }

        public int SolvePart1()
        {
            var result = 0;
            foreach (var row in Spreadsheet)
            {
                result += row.Max() - row.Min();
            }
            return result;
        }

        public int SolvePart2()
        {
            var result = 0;
            foreach (var row in Spreadsheet)
            {
                while (row.Count > 0)
                {
                    var actual = row.First();
                    row.RemoveAt(0);
                    var pair = row.FirstOrDefault(c => c % actual == 0 || actual % c == 0);
                    if (pair > 0)
                    {
                        result += actual > pair ? actual / pair : pair / actual;
                        break;
                    }
                }
            }
            return result;
        }
    }
}