using System.Linq;

namespace AdventOfCode2017
{
    public class Level4
    {
        public int SolvePart1()
        {
            var phrases = Inputs.Level4Input.Split('\n');
            var splitPhrases = phrases.Select(p => p.Replace('\r', ' ').Trim().Split(' '));
            var uniqueSplitPhrases = splitPhrases.Where(s => s.Length == s.Distinct().Count());
            return uniqueSplitPhrases.Count();
        }

        public int SolvePart2()
        {
            var phrases = Inputs.Level4Input.Split('\n');
            var splitPhrases = phrases.Select(p => p.Replace('\r', ' ').Trim().Split(' '));
            var orderedPhrases = splitPhrases.Select(s => s.Select(o => string.Concat(o.OrderBy(c => c))));
            var uniqueSplitPhrases = orderedPhrases.Where(s => s.Count() == s.Distinct().Count());
            return uniqueSplitPhrases.Count();
        }
    }
}