﻿using System;
using System.Linq;

namespace AdventOfCode2017
{
    public class Level5
    {
        private int[] level5Input;

        public Level5(string level5Input)
        {
            this.level5Input = level5Input.Replace("\n", "").Split('\r').Select(Int32.Parse).ToArray();
        }

        public int SolvePart1()
        {
            var position = 0;
            var steps = 0;
            while (position >= 0 && position < level5Input.Length)
            {
                var next = position + level5Input[position];
                level5Input[position]++;
                position = next;
                steps++;
            }
            return steps;
        }

        public int SolvePart2()
        {
            var position = 0;
            var steps = 0;
            while (position >= 0 && position < level5Input.Length)
            {
                var next = position + level5Input[position];
                if (level5Input[position] >= 3)
                {
                    level5Input[position]--;
                }
                else
                {
                    level5Input[position]++;
                }
                position = next;
                steps++;
            }
            return steps;
        }
    }
}
