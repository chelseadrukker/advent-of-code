﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2017
{
    class Program
    {
        static void Main(string[] args)
        {
            //var level1 = new Level1(Inputs.Level1Input);
            //Console.WriteLine($"Level 1 -> Step 1: {level1.SolvePart1(1)}");
            //Console.WriteLine($"Level 1 -> Step 2: {level1.SolvePart2()}");

            //var level2 = new Level2(Inputs.Level2Input);
            //Console.WriteLine($"Level 2 -> Step 1: {level2.SolvePart1()}");
            //Console.WriteLine($"Level 2 -> Step 2: {level2.SolvePart2()}");

            //var level3 = new Level3();
            //Console.WriteLine($"Level 3 -> Step 1: {level3.SolvePart1(368078)}");
            //Console.WriteLine($"Level 3 -> Step 2: {level3.SolvePart2(368078)}");

            //var level4 = new Level4();
            //Console.WriteLine($"Level 4 -> Step 1: {level4.SolvePart1()}");
            //Console.WriteLine($"Level 4 -> Step 2: {level4.SolvePart2()}");

            //var level5 = new Level5(Inputs.Level5Input);
            //Console.WriteLine($"Level 5 -> Step 1: {level5.SolvePart1()}");
            //level5 = new Level5(Inputs.Level5Input);
            //Console.WriteLine($"Level 5 -> Step 2: {level5.SolvePart2()}");

            //var level6 = new Level6(Inputs.Level6Input);
            //var level6Part1Result = level6.SolvePart1();
            //Console.WriteLine($"Level 6 -> Step 1: {level6Part1Result.Count - 1}");
            //level6 = new Level6(Inputs.Level6Input);
            //Console.WriteLine($"Level 6 -> Step 2: {level6.SolvePart2(level6Part1Result)}");

            var level7 = new Level7(Inputs.Level7Input);
            Console.WriteLine($"Level 7 -> Step 1: {level7.SolvePart1()}");

            level7 = new Level7(Inputs.Level7Input);
            var level7Part2Result = level7.SolvePart2();
            Console.WriteLine($"Level 7 -> Step 2: {level7Part2Result}");

            var level8 = new Level8(Inputs.Level8Input);
            Console.WriteLine($"Level 8 -> Step 1: {level8.SolvePart1()}");

            level8 = new Level8(Inputs.Level8Input);
            Console.WriteLine($"Level 8 -> Step 2: {level8.SolvePart2()}");

            Console.ReadLine();
        }
    }
}
