﻿using System.Net;

namespace AdventOfCode2017.Website
{
    public class Website : IWebsite
    {
        private readonly string _url;

        public Website(string url)
        {
            _url = url;
        }

        public string GetContent()
        {
            using (WebClient client = new WebClient())
            {
                return client.DownloadString(_url);
            }
        }
    }
}