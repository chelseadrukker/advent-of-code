﻿namespace AdventOfCode2017.Website
{
    internal interface IWebsite
    {
        string GetContent();
    }
}