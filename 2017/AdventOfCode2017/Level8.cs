﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace AdventOfCode2017
{
    internal class Level8
    {
        private ConcurrentDictionary<string, int> registers;
        private int max = int.MinValue;

        public Level8(string level8Input)
        {
            registers = new ConcurrentDictionary<string, int>();
            var lines = level8Input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                var parts = line.Split(' ');
                var registerKey = parts[0];
                var polarity = parts[1] == "inc" ? 1 : -1;
                var value = Int32.Parse(parts[2]);
                var referenced = parts[4];
                var comparator = parts[5];
                var compareValue = Int32.Parse(parts[6]);

                var updateValue = polarity * value;
                var shouldUpdate = comparator.Operator(registers.GetOrAdd(referenced, 0), compareValue);
                registers.GetOrAdd(registerKey, 0);
                registers.AddOrUpdate(registerKey, 0, (key, oldValue) => oldValue + (shouldUpdate ? updateValue : 0));
                max = max < registers.GetOrAdd(registerKey, 0) ? registers.GetOrAdd(registerKey, 0) : max;
            }
        }

        public int SolvePart1()
        {
            return registers.Values.Max();
        }

        public int SolvePart2()
        {
            return max;
        }


    }
}