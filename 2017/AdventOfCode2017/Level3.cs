﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Level3
    {
        private int layerGrowth = 8;

        public int SolvePart1(int targetPosition)
        {
            // Memory sample
            // 37  36  35  34  33  32  31 
            // 38  17  16  15  14  13  30
            // 39  18   5   4   3  12  29
            // 40  19   6   X  *2  11  28
            // 41  20   7   8   9 *10  27
            // 42  21  22  23  24  25 *26
            // 43  44  45  46  47  48  49

            if (targetPosition == 1) return 0;

            var layerIndex = 1;
            var layerMaximum = 9;
            var layerSize = 8;
            for (int i = 2; i <= targetPosition; i++)
            {
                if (i > layerMaximum)
                {
                    layerSize += layerGrowth;
                    layerMaximum += layerSize;
                    layerIndex++;
                }
            }
            var layerMinimum = layerMaximum - layerSize;
            var layerPosition = targetPosition - layerMinimum;
            var positionOnSide = layerPosition % (layerIndex * 2);
            var distanceFromCenter = Math.Abs(positionOnSide - layerIndex);

            return layerIndex + distanceFromCenter;
        }

        public int SolvePart2(int limit)
        {
            // 147  142  133  122   59
            // 304    5    4    2   57
            // 330   10    1    1   54
            // 351   11   23   25   26  1914
            // 362  747  806  880  931   957

            //     0       0
            // 0   5   4   2
            //    10   1   1   54
            // 0  11  23  25   26
            //     0       0   ...

            if (limit == 0) return 1;
            if (limit == 1) return 2;
            var layerIndex = 2;
            var previousSides = new List<List<int>>()
            {
                new List<int> { 0,  25,   1,   2 },
                new List<int> {      2,   4,   5 },
                new List<int> {      5,  10,  11 },
                new List<int> {     11,  23,  25 }
            };
            var current = 25;
            while (current <= limit)
            {
                var side = 0;
                while (side < 4)
                {
                    var newSide = new List<int>() { side % 4 == 0 ? 0 : previousSides[3].Last()};
                    var position = 0;
                    while (position < layerIndex * 2)
                    {
                        var innerLayerSum = previousSides[0].ElementAtOrDefault(position) + previousSides[0].ElementAtOrDefault(position + 1) + previousSides[0].ElementAtOrDefault(position + 2);
                        var previousElement = newSide.ElementAtOrDefault(position);
                        current = innerLayerSum + previousElement;
                        if (current > limit)
                        {
                            return current;
                        }
                        newSide.Add(current);
                        position++;
                    }
                    if (side % 4 == 0)
                    {
                        previousSides.Last().Add(newSide[1]);
                    }

                    previousSides.RemoveAt(0);
                    previousSides.Add(newSide);

                    if (side % 4 == 3)
                    {
                        previousSides[0][0] = newSide[newSide.Count - 1];
                        previousSides[0].Insert(0, 0);
                    }
                    else
                        previousSides[0].Insert(0, newSide[newSide.Count - 2]);
                    side++;
                }
                layerIndex++;
            }
            return current;
        }
    }
}