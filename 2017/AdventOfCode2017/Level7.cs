﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    public class Level7
    {
        public List<Node> nodes;

        public Level7(string level7Input)
        {
            var level7InputLines = level7Input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            this.nodes = level7InputLines.Select(i => new Node(i)).ToList();
        }

        public string SolvePart1()
        {
            string bottom = "";
            while (nodes.Any(n => n.children.Count > 0))
            {
                bottom = nodes.First(n => n.children.Any()).name;
                var leaves = nodes.Where(n => !n.children.Any()).Select(n => n.name).ToList();
                nodes.RemoveAll(n => !n.children.Any());
                foreach (var node in nodes)
                {
                    node.children.RemoveAll(c => leaves.Contains(c));
                }
                Console.Write("\rNodes remaining: {0} ", nodes.Count);
            }
            Console.WriteLine();
            return bottom;
        }

        public int SolvePart2()
        {
            while (nodes.Any(n => n.children.Count > 0))
            {
                var leaves = nodes.Where(n => !n.children.Any()).ToList();
                nodes.RemoveAll(n => !n.children.Any());
                foreach (var node in nodes)
                {
                    var kids = leaves.Where(l => node.children.Contains(l.name)).ToList();
                    if (kids.Select(k => k.weight + k.childrensWeight).Distinct().Count() > 1)
                    {
                        var bad = kids.First(l => leaves.Count(x => l.weight + l.childrensWeight == x.weight + x.childrensWeight) == 1);
                        var good = kids.First(l => l.name != bad.name);
                        var expected = good.weight + good.childrensWeight - bad.childrensWeight;
                        return expected;
                    }
                    node.childrensWeight += kids.Sum(l => l.weight + l.childrensWeight);
                    node.children.RemoveAll(c => leaves.Any(l => l.name == c));
                }
                Console.Write("\rNodes remaining: {0} ", nodes.Count);
            }
            Console.WriteLine();
            return 0;
        }
    }

    public class Node
    {
        public string name;
        public int level;
        public int weight;
        public List<string> children;
        public int childrensWeight = 0;

        public Node(string node)
        {
            var parts = node.Split(new[] {"->", "(", ")", ",", " "}, StringSplitOptions.RemoveEmptyEntries).ToList();
            this.name = parts[0];
            this.weight = Int32.Parse(parts[1]);
            this.children = parts.GetRange(2, parts.Count - 2);
        }
    }
}