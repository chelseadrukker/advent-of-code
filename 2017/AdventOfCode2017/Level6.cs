﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace AdventOfCode2017
{
    public class Level6
    {
        private int[] input;

        public Level6(string input)
        {
            this.input = input.Split('\t').Select(Int32.Parse).ToArray();
        }

        public List<int> SolvePart1()
        {
            int size = input.Length;
            var history = new List<int>();
            while (history.Distinct().Count() == history.Count)
            {
                history.Add(string.Join(",", input).GetHashCode());
                var maxPosition = Array.IndexOf(input, input.Max());
                var max = input[maxPosition];
                input[maxPosition] = 0;
                for (int i = 1; i <= size; i++)
                {
                    var current = (maxPosition + i) % size;
                    var division = max / size;
                    var moduloIfLeft = max % size >= i ? 1 : 0;
                    input[current] = input[current] + division + moduloIfLeft;
                }
                Console.Write("\rSteps made: {0} ", history.Count);
            }
            Console.WriteLine();
            return history;
        }

        public int SolvePart2(List<int> input)
        {
            return input.Count - input.IndexOf(input.Last()) - 1;
        }
    }
}
