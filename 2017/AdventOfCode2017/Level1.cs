﻿using System.Linq;

namespace AdventOfCode2017
{
    public class Level1
    {
        private readonly int[] _input;

        public Level1(string input)
        {
            _input = input.Select(c => int.Parse(c.ToString())).ToArray();
        }

        public int SolvePart2()
        {
            return SolvePart1(_input.Length / 2);
        }

        public int SolvePart1(int offset)
        {
            var result = 0;
            var inputLength = _input.Length;
            for(int pos = 0; pos < inputLength; pos++)
            {
                var current = _input[pos % inputLength];
                var other = _input[(pos + offset) % inputLength];
                result += current == other ? current : 0;
            }
            return result;
        }
    }
}