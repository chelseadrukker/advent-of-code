using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level1Tests
    {
        [TestMethod]
        public void Test0Matches()
        {
            var level1 = new Level1("1234");
            Assert.AreEqual(0, level1.SolvePart1(1));
        }

        [TestMethod]
        public void TestMatchInside()
        {
            var level1 = new Level1("1224");
            Assert.AreEqual(2, level1.SolvePart1(1));
        }

        [TestMethod]
        public void TestMatchOnEnds()
        {
            var level1 = new Level1("1231");
            Assert.AreEqual(1, level1.SolvePart1(1));
        }

        [TestMethod]
        public void TestMatchesInsideAndOnEnds()
        {
            var level1 = new Level1("123441");
            Assert.AreEqual(5, level1.SolvePart1(1));
        }

        [TestMethod]
        public void TestMultipleMatchesInsideAndOutside()
        {
            var level1 = new Level1("1144411");
            Assert.AreEqual(11, level1.SolvePart1(1));
        }
    }
}
