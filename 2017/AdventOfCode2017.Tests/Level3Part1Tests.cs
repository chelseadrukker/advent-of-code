using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level3Part1Tests
    {
        [TestMethod]
        public void Returns0For1()
        {
            var level3 = new Level3();
            Assert.AreEqual(0, level3.SolvePart1(1));
        }

        [DataTestMethod]
        [DataRow(2)]
        [DataRow(4)]
        [DataRow(6)]
        [DataRow(8)]
        public void Returns1(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(1, level3.SolvePart1(input));
        }

        [DataTestMethod]
        [DataRow(3)]
        [DataRow(5)]
        [DataRow(7)]
        [DataRow(9)]
        public void Returns2FromLevel1(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(2, level3.SolvePart1(input));
        }

        [DataTestMethod]
        [DataRow(11)]
        [DataRow(15)]
        [DataRow(19)]
        [DataRow(23)]
        public void Returns2FromLevel2(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(2, level3.SolvePart1(input));
        }

        [DataTestMethod]
        [DataRow(10)]
        [DataRow(12)]
        [DataRow(14)]
        [DataRow(16)]
        [DataRow(18)]
        [DataRow(20)]
        [DataRow(22)]
        [DataRow(24)]
        public void Returns3FromLevel2(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(3, level3.SolvePart1(input));
        }

        [DataTestMethod]
        [DataRow(13)]
        [DataRow(17)]
        [DataRow(21)]
        [DataRow(25)]
        public void Returns4FromLevel2(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(4, level3.SolvePart1(input));
        }

        [DataTestMethod]
        [DataRow(27)]
        [DataRow(29)]
        [DataRow(33)]
        [DataRow(35)]
        [DataRow(39)]
        [DataRow(41)]
        [DataRow(45)]
        [DataRow(47)]
        public void Returns4FromLevel3(int input)
        {
            var level3 = new Level3();
            Assert.AreEqual(4, level3.SolvePart1(input));
        }
    }
}
