using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level6Tests
    {
        [TestMethod]
        public void Part1ShouldFinishWithFiveStep()
        {
            var level6 = new Level6("0\t2\t7\t0");
            Assert.AreEqual(5, level6.SolvePart1().Count - 1);
        }

        [TestMethod]
        public void Part2ShouldReturnFourSteps()
        {
            var level6 = new Level6("0\t2\t7\t0");
            Assert.AreEqual(4, level6.SolvePart2(level6.SolvePart1()));
        }
    }
}