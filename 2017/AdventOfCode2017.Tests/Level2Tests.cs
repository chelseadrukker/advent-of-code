using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level2Tests
    {
        [TestMethod]
        public void ConstructorReadsMatrix()
        {
            var level2 = new Level2("1\t2\t3\t4\r\n1\t2\t3\t4\r\n1\t2\t3\t4\r\n1\t2\t3\t4");
            Assert.AreEqual(1, level2.Spreadsheet.First().First());
            Assert.AreEqual(1, level2.Spreadsheet.Last().First());
            Assert.AreEqual(4, level2.Spreadsheet.Last().Last());
            Assert.AreEqual(4, level2.Spreadsheet.First().Last());
        }

        [TestMethod]
        public void CalculatesChecksum()
        {
            var level2 = new Level2("5\t1\t9\t5\r\n7\t5\t3\r\n2\t4\t6\t8");
            Assert.AreEqual(18, level2.SolvePart1());
        }

        [TestMethod]
        public void CalculatesEvenDivisionChecksum()
        {
            var level2 = new Level2("5\t9\t2\t8\r\n9\t4\t7\t3\r\n3\t8\t6\t5");
            Assert.AreEqual(9, level2.SolvePart2());
        }
    }
}
