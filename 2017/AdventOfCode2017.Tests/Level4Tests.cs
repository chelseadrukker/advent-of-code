using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level4Tests
    {
        [TestMethod]
        public void Part1ShouldReturn466()
        {
            var level4 = new Level4();
            Assert.AreEqual(466, level4.SolvePart1());
        }
        [TestMethod]
        public void Part2ShouldReturn251()
        {
            var level4 = new Level4();
            Assert.AreEqual(251, level4.SolvePart2());
        }
    }
}
