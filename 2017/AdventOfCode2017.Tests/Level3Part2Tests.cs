using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level3Part2Tests
    {
        [TestMethod]
        public void Returns1For0()
        {
            var level3 = new Level3();
            Assert.AreEqual(1, level3.SolvePart2(0));
        }

        [TestMethod]
        public void Returns2For1()
        {
            var level3 = new Level3();
            Assert.AreEqual(2, level3.SolvePart2(1));
        }

        [TestMethod]
        public void Returns26For25()
        {
            var level3 = new Level3();
            Assert.AreEqual(26, level3.SolvePart2(25));
        }

        [TestMethod]
        public void Returns54For26()
        {
            var level3 = new Level3();
            Assert.AreEqual(54, level3.SolvePart2(26));
        }

        [TestMethod]
        public void Returns57For54()
        {
            var level3 = new Level3();
            Assert.AreEqual(57, level3.SolvePart2(54));
        }

        [TestMethod]
        public void Returns59For57()
        {
            var level3 = new Level3();
            Assert.AreEqual(59, level3.SolvePart2(57));
        }

        [TestMethod]
        public void Returns122For59()
        {
            var level3 = new Level3();
            Assert.AreEqual(122, level3.SolvePart2(59));
        }

        [TestMethod]
        public void Returns147For142()
        {
            var level3 = new Level3();
            Assert.AreEqual(147, level3.SolvePart2(142));
        }

        [TestMethod]
        public void Returns931For880()
        {
            var level3 = new Level3();
            Assert.AreEqual(931, level3.SolvePart2(880));
        }

        [TestMethod]
        public void Returns957For931()
        {
            var level3 = new Level3();
            Assert.AreEqual(957, level3.SolvePart2(931));
        }

        [TestMethod]
        public void Returns1968For957()
        {
            var level3 = new Level3();
            Assert.AreEqual(1968, level3.SolvePart2(957));
        }
    }
}
