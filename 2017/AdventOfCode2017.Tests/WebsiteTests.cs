using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class WebsiteTests
    {
        [TestMethod]
        public void WebsiteReturnsString()
        {
            var website = new Website.Website("http://www.google.com");
            Assert.IsFalse(string.IsNullOrEmpty(website.GetContent()));
        }
    }
}
