using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level5Tests
    {
        [TestMethod]
        public void Part1ShouldFinishWithOneStep()
        {
            var level5 = new Level5("1");
            Assert.AreEqual(1, level5.SolvePart1());
        }

        [TestMethod]
        public void Part1ShouldFinishWithFiveStep()
        {
            var level5 = new Level5("0\r\n3\r\n0\r\n1\r\n-3");
            Assert.AreEqual(5, level5.SolvePart1());
        }
    }
}
