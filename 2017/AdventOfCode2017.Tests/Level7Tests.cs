using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AdventOfCode2017.Tests
{
    [TestClass]
    public class Level7Tests
    {
        [TestMethod]
        public void ShouldRead12Lines()
        {
            var level7 = new Level7("pbga (66)\r\nxhth (57)\r\nebii (61)\r\nhavc (66)\r\nktlj (57)\r\nfwft (72) -> ktlj, cntj, xhth\r\nqoyq (66)\r\npadx (45) -> pbga, havc, qoyq\r\ntknk (41) -> ugml, padx, fwft\r\njptl (61)\r\nugml (68) -> gyxo, ebii, jptl\r\ngyxo (61)\r\ncntj (57)");
            Assert.AreEqual(13, level7.nodes.Count());
        }

        [TestMethod]
        public void ShouldReadLeaveNodeLines()
        {
            var level7 = new Level7("pbga (66)\r\nxhth (57)\r\nebii (61)\r\nhavc (66)\r\nktlj (57)\r\nfwft (72) -> ktlj, cntj, xhth\r\nqoyq (66)\r\npadx (45) -> pbga, havc, qoyq\r\ntknk (41) -> ugml, padx, fwft\r\njptl (61)\r\nugml (68) -> gyxo, ebii, jptl\r\ngyxo (61)\r\ncntj (57)");
            Assert.AreEqual("pbga", level7.nodes.First().name);
            Assert.AreEqual(66, level7.nodes.First().weight);
        }

        [TestMethod]
        public void ShouldReadParentNodeLines()
        {
            var level7 = new Level7("pbga (66)\r\nxhth (57)\r\nebii (61)\r\nhavc (66)\r\nktlj (57)\r\nfwft (72) -> ktlj, cntj, xhth\r\nqoyq (66)\r\npadx (45) -> pbga, havc, qoyq\r\ntknk (41) -> ugml, padx, fwft\r\njptl (61)\r\nugml (68) -> gyxo, ebii, jptl\r\ngyxo (61)\r\ncntj (57)");
            Assert.AreEqual("ktlj", level7.nodes.First(n => n.name == "fwft").children.First());
            Assert.AreEqual("xhth", level7.nodes.First(n => n.name == "fwft").children.Last());
            Assert.AreEqual(3, level7.nodes.First(n => n.name == "fwft").children.Count);
        }

        [TestMethod]
        public void ShouldReturnBottomProgram()
        {
            var level7 = new Level7("pbga (66)\r\nxhth (57)\r\nebii (61)\r\nhavc (66)\r\nktlj (57)\r\nfwft (72) -> ktlj, cntj, xhth\r\nqoyq (66)\r\npadx (45) -> pbga, havc, qoyq\r\ntknk (41) -> ugml, padx, fwft\r\njptl (61)\r\nugml (68) -> gyxo, ebii, jptl\r\ngyxo (61)\r\ncntj (57)");
            Assert.AreEqual("tknk", level7.SolvePart1());
        }

        [TestMethod]
        public void ShouldReturn60()
        {
            var level7 = new Level7("pbga (66)\r\nxhth (57)\r\nebii (61)\r\nhavc (66)\r\nktlj (57)\r\nfwft (72) -> ktlj, cntj, xhth\r\nqoyq (66)\r\npadx (45) -> pbga, havc, qoyq\r\ntknk (41) -> ugml, padx, fwft\r\njptl (61)\r\nugml (68) -> gyxo, ebii, jptl\r\ngyxo (61)\r\ncntj (57)");
            Assert.AreEqual(60, level7.SolvePart2());
        }

        [TestMethod]
        public void ShouldReturn1060()
        {
            var level7 = new Level7(Inputs.Level7Input);
            Assert.AreEqual(1060, level7.SolvePart2());
        }
    }
}