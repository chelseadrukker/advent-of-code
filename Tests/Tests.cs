﻿using NUnit.Framework;
using System;
using System.IO;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    public class Tests
    {
        [Test]
        public void Test()
        {
            Assert.That(1, Is.EqualTo(1));
        }
    }
}
