import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt',
    'workers': 5,
    'time': 60
}


def main():
    strings = read_input_as_string_list()
    moves = strings_to_moves(strings)
    ends = [m[1] for m in moves]
    starts = [m[0] for m in moves]
    states = [(state, dependencies(state, moves))
              for state in set(ends).union(set(starts))]
    processed = []
    available = available_states(states, processed)
    tick = 0
    workers = []
    while(len(workers) > 0 or len(states) > 0):
        tick += 1
        processed += [worker[0] for worker in workers if worker[1] == 0]
        workers = [[worker[0], worker[1] - 1]
                   for worker in workers if worker[1] > 0]
        available = available_states(states, processed)
        while(len(workers) < CONFIG['workers'] and len(available) > 0):
            state_to_process = available[0][0]
            workers.append(
                [state_to_process, time_to_process_state(available[0])])
            states = [state for state in states if state[0]
                      != state_to_process]
            available = available[1:]
        print(workers)
    print(tick-1)
    return tick-1


def time_to_process_state(state):
    return CONFIG['time'] + ord(state[0]) - ord('A')


def available_states(states, processed):
    return list(sorted([state for state in states if all_dependencies_are_processed(state, processed)]))


def all_dependencies_are_processed(state, processed):
    return len(set(state[1]) - set(processed)) == 0


def dependencies(state, moves):
    return [x[0] for x in moves if x[1] == state]


def strings_to_moves(strings):
    return [[g for g in re.search('.*([A-Z]).*([A-Z]).*', s).groups()] for s in strings]


def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], CONFIG['input_file']))]


main()


class Tests(unittest.TestCase):
    def test_rectangle(self):
        self.assertEqual(main(), 15, 'incorrect pair detection')

    def test_time(self):
        self.assertEqual(time_to_process_state(
            ['A']), 1, "wrong time calculated")


if __name__ == '__main__':
    unittest.main()
