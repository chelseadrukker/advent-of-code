from approvaltests.reporters.generic_diff_reporter import GenericDiffReporter
from approvaltests.approvals import verify
from target_finder import find_path_to_nearest_target
from coordinate import Coordinate
from tile import Tile
import constants
import os
import sys
import unittest
import collections

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    rows = read_input_as_string_list()
    width = len(rows[0])
    height = len(rows)
    stop=False
    elf_ad = 3
    while(not stop):
        elf_ad += 1
        tiles = parse_tiles(rows, elf_ad)
        board = Board(tiles, width, height, 0, elf_ad=elf_ad)
        elfs_count = len([tile for tile in board.tiles if tile.tile_type == constants.ELF])
        while(not finished(board)):
            board = board.next_turn()
            # print(board)
            # print(board.generation, [tile.hp for tile in board.tiles if tile.tile_type in constants.CREATURE_TILES])
        print(board.generation * sum([tile.hp for tile in board.tiles if tile.tile_type in constants.CREATURE_TILES]))
        after = len([tile for tile in board.tiles if tile.tile_type == constants.ELF])
        if elfs_count == after:
            stop = True
            print(elf_ad)

def finished(board):
    return constants.GOBLIN not in str(board) or constants.ELF not in str(board)

def parse_tiles(rows, elf_ad):
    tiles = []
    for y, row in enumerate(rows):
        for x, c in enumerate(row):
            tile = TileFactory().create(c, x, y, elf_ad=elf_ad)
            tiles.append(tile)
    return tiles


def is_in_range(base, target):
    b = base.coordinate
    t = target.coordinate
    if(b == t):
        raise Exception('Base and target can not be on same coordinate!')
    r = base.attack_range
    h_range = b.x == t.x and (abs(b.y - t.y) <= r)
    v_range = b.y == t.y and (abs(b.x - t.x) <= r)
    return h_range or v_range


class Board(object):
    def __init__(self, tiles, width, height, generation, elf_ad):
        self.tiles, x = self.reset_if_needed(tiles)
        self.width = width
        self.height = height
        self.generation = generation + x
        self.elf_ad = elf_ad

    def __str__(self):
        tiles = sorted(self.tiles, key=lambda tile: (
            tile.coordinate.y, tile.coordinate.x))
        rows = chunks([str(tile) for tile in tiles], self.width)
        return '\n'.join([''.join(row) for row in rows])

    def next_turn(self):
        # sort tiles by row and col priority
        tiles = sorted(self.tiles, key=lambda tile: (
            tile.coordinate.y, tile.coordinate.x))

        # identify attacker
        attacker = self.next_active_creature(tiles)
        tiles.remove(attacker)

        # identify target
        # !!! creatures does not attack own race
        target_type = constants.GOBLIN if attacker.tile_type == constants.ELF else constants.ELF
        
        path = find_path_to_nearest_target(
            attacker, target_type, constants.BLOCKING_TILES, tiles.copy())

        # can not move towards enemies
        if(path == None):
            attacker.turn_made = True
            tiles.append(attacker)
            return Board(tiles, self.width, self.height, self.generation, elf_ad=self.elf_ad)

        # target_coordinate = path[-1]
        
        # target = [tile for tile in tiles if tile.coordinate ==
        #           target_coordinate][0]
        # tiles.remove(target)

        # move towards the target if not in range
        if(len(path) > 2):
            destination = path[1]
            dt = next(tile for tile in tiles if tile.coordinate == destination)
            dt.coordinate = attacker.coordinate
            attacker.coordinate = destination

        targets_in_range = attacker.targets_in_range(tiles, target_type)
        targets_in_range = sorted(targets_in_range, key=lambda tile: (
            tile.hp, tile.coordinate.y, tile.coordinate.x))
        if(len(targets_in_range) > 0):
            target = targets_in_range[0]
            tiles.remove(target)
            attacker.attack(target)
            if(target.is_dead()):
                target = TileFactory().create(
                    constants.EMPTY, target.coordinate.x, target.coordinate.y)
            tiles.append(target)

        attacker.turn_made = True

        tiles.append(attacker)

        return Board(tiles, self.width, self.height, self.generation, elf_ad=self.elf_ad)

    def next_active_creature(self, tiles):
        return next(tile for tile in tiles if (tile.is_creature() and not tile.turn_made))

    def reset_if_needed(self, tiles):
        if(self.no_turns_left(tiles)):
            for tile in tiles:
                tile.turn_made = False
            return tiles, 1
        return tiles, 0

    def no_turns_left(self, tiles):
        return next((tile for tile in tiles if (tile.is_creature() and not tile.turn_made)), None) == None


class TileFactory(object):
    def create(self, tile_type, x, y, elf_ad=3):
        return {
            constants.ELF: Tile(constants.ELF, x, y, 200, elf_ad, 1),
            constants.GOBLIN: Tile(constants.GOBLIN, x, y, 200, 3, 1),
            constants.WALL: Tile(constants.WALL, x, y),
            constants.EMPTY: Tile(constants.EMPTY, x, y),
        }[tile_type]


# class AllTests(unittest.TestCase):
#     def setUp(self):
#         self.reporter = GenericDiffReporter(
#             ('TortoiseGitMerge', 'C:/Program Files/TortoiseGit/bin/TortoiseGitMerge.exe'))

#     def test_elf_print(self):
#         verify(str(TileFactory().create(constants.ELF, 0, 0)), self.reporter)

#     def test_goblin_print(self):
#         verify(str(TileFactory().create(constants.GOBLIN, 0, 0)), self.reporter)

#     def test_board_printing(self):
#         rows = read_input_as_string_list()
#         tiles = parse_tiles(rows)
#         width = len(rows[0])
#         height = len(rows)
#         verify(str(Board(tiles, width, height)), self.reporter)

#     def test_goblin_dies(self):
#         tiles = [TileFactory().create(constants.ELF, 1, 0),
#                  Tile(constants.GOBLIN, 2, 0, 0)]
#         verify(str(Board(tiles, 2, 1).next_turn()), self.reporter)

#     # TODO refactor duplicate code
#     def test_target_is_in_range_on_left(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 0, 1),
#                                TileFactory().create(constants.GOBLIN, 0, 0))), self.reporter)

#     def test_target_is_in_range_on_right(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 0, 1),
#                                TileFactory().create(constants.GOBLIN, 0, 2))), self.reporter)

#     def test_target_is_in_range_above(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 1, 0),
#                                TileFactory().create(constants.GOBLIN, 0, 0))), self.reporter)

#     def test_target_is_in_range_below(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 1, 0),
#                                TileFactory().create(constants.GOBLIN, 2, 0))), self.reporter)

#     def test_target_is_not_in_range_two_to_left(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 0, 1),
#                                TileFactory().create(constants.GOBLIN, 0, -1))), self.reporter)

#     def test_target_is_not_in_range_two_to_right(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 0, 1),
#                                TileFactory().create(constants.GOBLIN, 0, 3))), self.reporter)

#     def test_target_is_not_in_range_two_above(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 1, 0),
#                                TileFactory().create(constants.GOBLIN, -1, 0))), self.reporter)

#     def test_target_is_not_in_range_two_below(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 1, 0),
#                                TileFactory().create(constants.GOBLIN, 3, 0))), self.reporter)

#     def test_target_is_not_in_range_diagonally(self):
#         verify(str(is_in_range(TileFactory().create(constants.ELF, 0, 0),
#                                TileFactory().create(constants.GOBLIN, 1, 1))), self.reporter)

#     def test_target_is_in_range_on_border(self):
#         verify(str(is_in_range(Tile(constants.ELF, 0, 0, attack_range=3),
#                                TileFactory().create(constants.GOBLIN, 0, 3))), self.reporter)

#     def test_target_is_in_range_inside_border(self):
#         verify(str(is_in_range(Tile(constants.ELF, 0, 0, attack_range=3),
#                                TileFactory().create(constants.GOBLIN, 0, 1))), self.reporter)

#     def test_target_is_on_same_position(self):
#         message = ''
#         try:
#             is_in_range(TileFactory().create(constants.ELF, 0, 0),
#                         TileFactory().create(constants.GOBLIN, 0, 0))
#         except Exception as e:
#             message = str(e)
#         verify(message, self.reporter)

#     def test_target_does_not_get_attacked(self):
#         tiles = [Tile(constants.ELF, 1, 0, ad=15, attack_range=1),
#                  Tile(constants.GOBLIN, 3, 0, hp=10)]
#         verify(str(Board(tiles, 2, 1).next_turn()), self.reporter)

#     def test_target_gets_attacked(self):
#         tiles = [Tile(constants.ELF, 1, 0, ad=15, attack_range=1),
#                  Tile(constants.GOBLIN, 2, 0, hp=10)]
#         verify(str(Board(tiles, 2, 1).next_turn()), self.reporter)

#     def test_flagged_are_ignored(self):
#         tiles = [Tile(constants.GOBLIN, 0, 0, turn_made=True),
#                  Tile(constants.ELF, 1, 0, turn_made=True),
#                  Tile(constants.ELF, 2, 0, ad=15, attack_range=1),
#                  Tile(constants.GOBLIN, 3, 0, hp=10)]
#         next_turn = Board(tiles, 4, 1).next_turn()
#         positions = str([str(tile.coordinate) for tile in next_turn.tiles])
#         verify(str(next_turn) + '\n' + positions, self.reporter)

#     def test_flags_are_not_clared_until_last_turn(self):
#         tiles = [Tile(constants.GOBLIN, 0, 0, turn_made=True),
#                  Tile(constants.ELF, 1, 0),
#                  Tile(constants.ELF, 2, 0)]
#         next_turn = Board(tiles, 2, 1).next_turn()
#         verify(str([tile.turn_made for tile in next_turn.tiles]), self.reporter)

#     def test_flags_are_clared_if_all_flagged(self):
#         tiles = [Tile(constants.GOBLIN, 0, 0, turn_made=True),
#                  Tile(constants.ELF, 1, 0, turn_made=True),
#                  Tile(constants.ELF, 2, 0, turn_made=True),
#                  Tile(constants.ELF, 3, 0, turn_made=True)]
#         next_turn = Board(tiles, 4, 1).next_turn()
#         verify(str([tile.turn_made for tile in next_turn.tiles]), self.reporter)


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]


main()

if __name__ == '__main__':
    unittest.main()
