from approvaltests.reporters.generic_diff_reporter import GenericDiffReporter
from approvaltests.approvals import verify
from coordinate import Coordinate
from tile import Tile
import unittest
import constants


def find_path_to_nearest_target(attacker, target_type, blocking_tiles, tiles):
    paths = [[attacker.coordinate]]
    found = False
    while(not found):
        new_paths = []
        for path in paths:
            for nb in path[-1].neighbours():
                # if tile on neighbour is empty or enemy add it to list
                # if its enemy, end search and return last path
                nb_tile = next(
                    (tile for tile in tiles if tile.coordinate == nb), None)
                if(nb_tile != None and nb_tile.tile_type in [constants.EMPTY, target_type]):
                    tiles.remove(nb_tile)
                    new_paths.append(path + [nb])
                    if(nb_tile.tile_type == target_type):
                        found = True
                        break
            if(found):
                break
        if(paths == new_paths and not found):
            return None
        paths = new_paths
    return paths[-1]
    # return [tile.coordinate for tile in tiles]





class PathFinderTests(unittest.TestCase):
    def setUp(self):
        self.reporter = GenericDiffReporter(
            ('TortoiseGitMerge', 'C:/Program Files/TortoiseGit/bin/TortoiseGitMerge.exe'))

    def test_neighbours(self):
        nb = [str(n)
              for n in Coordinate(10, 10).neighbours()]
        verify(str(nb), self.reporter)

    def test_simple_path(self):
        tiles = [Tile(constants.EMPTY, 1, 0),
                 Tile(constants.EMPTY, 2, 0),
                 Tile(constants.ELF, 3, 0, turn_made=True)]
        path = find_path_to_nearest_target(
            Tile(constants.GOBLIN, 0, 0), constants.ELF, constants.BLOCKING_TILES, tiles)
        verify(str([str(coord) for coord in path]), self.reporter)

    def test_other_path(self):
        tiles = [Tile(constants.WALL, 1, 1),
                 Tile(constants.EMPTY, 0, 0),
                 Tile(constants.EMPTY, 1, 0),
                 Tile(constants.EMPTY, 2, 0),
                 Tile(constants.EMPTY, 0, 2),
                 Tile(constants.EMPTY, 1, 2),
                 Tile(constants.EMPTY, 2, 2),
                 Tile(constants.ELF, 2, 1, turn_made=True)]
        path = find_path_to_nearest_target(
            Tile(constants.GOBLIN, 0, 1), constants.ELF, constants.BLOCKING_TILES, tiles)
        verify(str([str(coord) for coord in path]), self.reporter)


if __name__ == '__main__':
    unittest.main()
