class Coordinate(object):
    def __init__(self, x, y):
        '''Defines x and y variables'''
        self.x = x
        self.y = y

    def __str__(self):
        return ', '.join([str(self.x), str(self.y)])

    def __eq__(self, value):
        return self.x == value.x and self.y == value.y

    def neighbours(self):
        return [Coordinate(self.x, self.y - 1),
            Coordinate(self.x - 1, self.y),
            Coordinate(self.x + 1, self.y),
            Coordinate(self.x, self.y + 1)]