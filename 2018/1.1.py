import sys
import os

frequencies = list(map(lambda frequency: int(frequency.rstrip('\n')), list(open(os.path.join(sys.path[0], '1.input.txt')))))
seen = {0}
current = 0
for i in range(1000):
    for frequency in frequencies:
        current += frequency
        if(current in seen):
            print(current)
            sys.exit()
        seen.add(current)
