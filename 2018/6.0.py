import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


class Point:
    def __init__(self, c):
        self.x = c[0]
        self.y = c[1]

    def __repr__(self):
        return "({0}, {1})".format(self.x, self.y)

    def __eq__(self, obj):
        return isinstance(obj, Point) and obj.x == self.x and obj.y == self.y

    def __hash__(self):
        return super().__hash__()


def main():
    coordinates = coordinates_from_input()
    points = all_points(coordinates)
    with_closest = {p: closest(p, coordinates) for p in points}
    without_draws = {k: v for k, v in with_closest.items() if v != None}
    coordinates_with_points = [(c, [k for k, v in without_draws.items() if v[0] == c]) for c in coordinates]
    filtered_touching_edges = without_touching_edges(coordinates_with_points, *boundaries(coordinates))
    result = [len(x[1]) for x in filtered_touching_edges]
    print(max(result))

def without_touching_edges(coordinates_with_points, min_x, max_x, min_y, max_y):
    return [x for x in coordinates_with_points if all(list(map(lambda p: p.x != min_x and p.x != max_x and p.y != min_y and p.y != max_y, x[1])))]

def closest(origo, coordinates):
    with_distance = [(c, distance(origo, c)) for c in coordinates]
    shortest = min([x[1] for x in with_distance])
    closest = list(filter(lambda x: x[1] == shortest, with_distance))
    number_of_closest = len(closest)
    return None if number_of_closest > 1 else closest[0]


def distance(origo, coordinate):
    return abs(origo.x - coordinate.x) + abs(origo.y - coordinate.y)


def coordinates_from_input():
    lines = read_input_as_string_list()
    pairs = [l.replace(' ', '').split(',') for l in lines]
    coordinates = coordinates_to_points([[int(x) for x in p] for p in pairs])
    return coordinates


def all_points(coordinates):
    min_x, max_x, min_y, max_y = boundaries(coordinates)
    return coordinates_to_points(list(itertools.product(range(min_x, max_x + 1), range(min_y, max_y + 1))))

def boundaries(coordinates):
    xes = [c.x for c in coordinates]
    min_x = min(xes)
    max_x = max(xes)
    ys = [c.y for c in coordinates]
    min_y = min(ys)
    max_y = max(ys)
    return min_x, max_x, min_y, max_y


def coordinates_to_points(coordinates):
    return [Point(c) for c in coordinates]


def flatten(nested):
    return list(itertools.chain.from_iterable(nested))


def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], CONFIG['input_file']))]


main()

# Tests


class Tests(unittest.TestCase):
    def test_rectangle(self):
        self.assertEqual(all_points([Point((2, 2)), Point((3, 3))]),
                         [
            Point((2, 2)), Point((2, 3)),
            Point((3, 2)), Point((3, 3))
        ],
            'incorrect pair detection')
        self.assertEqual(all_points([Point((5, 5)), Point((3, 3))]),
                         [
            Point((3, 3)), Point((3, 4)), Point((3, 5)),
            Point((4, 3)), Point((4, 4)), Point((4, 5)),
            Point((5, 3)), Point((5, 4)), Point((5, 5)),
        ],
            'incorrect rectangle generation')


if __name__ == '__main__':
    unittest.main()
