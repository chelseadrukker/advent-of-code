import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    limit = 50000000000
    offset = 0
    lines = read_input_as_string_list()
    generation = lines[0]
    rules = lines[1:]
    rules = changing_rules(rules)
    while(limit > 0):
        generation, offset = fix_offset(generation, offset)
        next_gen = ".."
        for i in range(2, len(generation) - 2):
            next_gen += generation[i] if leave(generation[i-2:i+3], rules) else opposite(generation[i])
        slider = generation.strip('.') == next_gen.strip('.')
        generation = next_gen
        limit -= 1
        print(limit, ':', generation, offset)
        positions = [i - offset for i, c in enumerate(generation) if c == '#']
        if(slider):
            print(sum(positions) + generation.count('#') * limit)
            break
        

def opposite(c):
    return '#' if c == '.' else '.'

def leave(part, rules):
    return part not in rules

def fix_offset(generation, offset):
    start = re.match(r'(^\.*)', generation).groups()[0]
    to_start = missing(start)
    end = re.search(r'(\.*$)', generation).groups()[0]
    to_end = missing(end)
    if(to_start > 0):
        generation = to_start * '.' + generation
    else:
        generation = generation[abs(to_start):]
    if(to_end > 0):
        generation = generation + to_end * '.'
    else:
        generation = generation[:to_end]
    return generation, to_start + offset

def missing(part):
    to_add = 0
    empty_pots = len(part)
    to_add = 5-empty_pots
    return to_add


def changing_rules(rules):
    return [r[0:5] for r in rules if r[2] != r[9]]


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
