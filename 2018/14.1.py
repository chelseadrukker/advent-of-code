from collections import deque
import os
import unittest
from timeit import default_timer as timer

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    current_receipe = ([3, 7], 0, 1)
    search = '607331'
    stop = False
    pos = 0
    while(not stop):
        start = timer()
        for i in range(100000):
            current_receipe=calculate_new_receipe(*current_receipe)
        end = timer()
        print(end - start) 
        pos = ''.join(str(c) for c in current_receipe[0][-100002-len(search):]).find(search)
        if(pos > 0):
            stop = True
    print(''.join(str(c) for c in current_receipe[0]).find(search))


def calculate_new_receipe(receipe, pos1, pos2):
    v1=receipe[pos1]
    v2=receipe[pos2]
    new_receipe = [int(n) for n in str(v1 + v2)]
    receipe += new_receipe
    length=len(receipe)
    return receipe, (pos1 + v1 + 1) % length, (pos2 + v2 + 1) % length

start = timer()
main()
end = timer()
print(end - start) 


class Tests(unittest.TestCase):
    def test_new_receipe_is_appended(self):
        receipe='39'
        pos1=0
        pos2=1
        self.assertEqual(calculate_new_receipe(receipe, pos1, pos2)[0], '3912',
                         'wrong appending')

    def test_second_new_receipe_gets_appended(self):
        receipe='37'
        pos1=0
        pos2=1
        next_round=calculate_new_receipe(receipe, pos1, pos2)
        self.assertEqual(next_round, ('3710', 0, 1),
                         'wrong first appending')
        next_round=calculate_new_receipe(*next_round)
        self.assertEqual(next_round, ('371010', 4, 3),
                         'wrong second appending')
        next_round=calculate_new_receipe(*next_round)
        self.assertEqual(next_round, ('3710101', 6, 4),
                         'wrong third appending')
        next_round=calculate_new_receipe(*next_round)
        self.assertEqual(next_round, ('37101012', 0, 6),
                         'wrong fourth appending')

    def test_first_elfs_goes_to_the_same_position(self):
        receipe='37'
        pos1=0
        pos2=1
        self.assertEqual(calculate_new_receipe(receipe, pos1, pos2)[1], 0,
                         'first elf had to stay on the same position')

    def test_first_elfs_goes_to_other_position(self):
        receipe='47'
        pos1=0
        pos2=1
        self.assertEqual(calculate_new_receipe(receipe, pos1, pos2)[1], 1,
                         'first elf had to move')

    def test_second_elfs_goes_to_the_same_position(self):
        receipe='37'
        pos1=0
        pos2=1
        self.assertEqual(calculate_new_receipe(receipe, pos1, pos2)[2], 1,
                         'second elf had to stay on the same position')

    def test_second_elfs_goes_to_other_position(self):
        receipe='46'
        pos1=0
        pos2=1
        self.assertEqual(calculate_new_receipe(receipe, pos1, pos2)[2], 0,
                         'second elf had to move')


# if __name__ == '__main__':
    # unittest.main()
