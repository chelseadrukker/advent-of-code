import collections
import itertools
import os
import re
import sys


def main():
    strings = sorted(read_input_as_string_list())

    minutes_and_ids = list(
        map(lambda i: str(re.search('.*([:|#][0-9]+)', i).group(1)), strings))

    results = {}
    minutes = []
    current = ""
    for entry in minutes_and_ids:
        if entry[0] == '#':
            if len(minutes) > 0:
                results.setdefault(current, []).append(
                    flatten(generate_ranges(minutes)))
                minutes.clear()
            current = entry[1:]
        else:
            minutes.append(int(entry[1:]))

    for minute in range(0,60):
        best = -1
        best_len = 0
        temp = results.copy()
        while len(temp) > 0:
            it = temp.popitem()
            flat = flatten(it[1])
            filtered = list(filter(lambda f: f == minute, flat))
            if(len(filtered) > best_len):
                best = it[0]
                best_len = len(filtered)

        print(minute, int(best), int(minute), int(best)*int(minute), best_len)


def most_common(lst):
    return max(set(lst), key=lst.count)


def flatten(nested):
    return list(itertools.chain.from_iterable(nested))


def generate_ranges(minutes):
    return list(map(lambda x: list(range(x[0], x[1])), zip(minutes[0::2], minutes[1::2])))


def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], '4.input.txt'))]


main()
