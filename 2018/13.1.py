import collections
import itertools
import os
import re
import sys
import unittest
from datetime import datetime

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}

start = datetime.now().second


def main():
    raw_track = read_input_as_string_list()
    clean_track = clear_track(raw_track)
    carts = parse_carts(raw_track, clean_track)
    game = Game(list(carts))
    tick = True
    while(tick):
        game = game.tick()
        # print([(cart.x, cart.y, cart.direction) for cart in game.carts])
        if len(game.carts) == 1:
            print('Last: ' + str(game.carts[0].x) + ',' + str(game.carts[0].y))
            tick = False


class Game:
    def __init__(self, carts):
        """ Create new game """
        self.carts = sorted(carts, key = lambda cart: (cart.x, cart.y))

    def tick(self):
        # find carts to be removed before next generation
        cart_combinations = itertools.combinations(self.carts, 2)
        crashing_carts = []
        for cart_combination in cart_combinations:
            if(they_will_crash(cart_combination)):
                first = (cart_combination[0].x, cart_combination[0].y)
                second = (cart_combination[1].x, cart_combination[1].y)
                if(first not in crashing_carts and second not in crashing_carts):
                    crashing_carts.append(first)
                    crashing_carts.append(second)
        
        # create next generation without carts crashing
        next_game = Game([cart.move() for cart in self.carts if (
            cart.x, cart.y) not in crashing_carts])
        # remove carts from next generation being on same place
        next_positions = [(cart.x, cart.y) for cart in next_game.carts]
        crashed_after_move = [x for n, x in enumerate(
            next_positions) if x in next_positions[:n]]
        next_game.carts = [cart for cart in next_game.carts if (
            cart.x, cart.y) not in crashed_after_move]

        if(len(self.carts) > len(next_game.carts)):
            before = [(cart.x, cart.y, cart.direction)
                      for cart in self.carts]
            after = [(cart.x, cart.y, cart.direction)
                     for cart in next_game.carts]
            print('before: ', before)
            print('after: ', after)
            crashes = crashing_carts + crashed_after_move
            print('crashes: ', len(crashes), crashes)
        return next_game


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


def clear_track(raw_track):
    return [t.replace('^', '|').replace('v', '|').replace('<', '-').replace('>', '-') for t in raw_track]


def parse_carts(raw_track, clear_track):
    for y, row in enumerate(raw_track):
        for x, col in enumerate(row):
            if col in '<^>v':
                yield Cart(x, y, col, clear_track)  # ['<','^','>','v']


def they_will_crash(pair):
    pair = sorted(pair, key = lambda cart: (cart.x, cart.y))
    cart1 = pair[0]
    cart2 = pair[1]
    dir1 = cart1.direction
    dir2 = cart2.direction
    x1 = cart1.x
    x2 = cart2.x
    y1 = cart1.y
    y2 = cart2.y
    crashes = [
        '><-10',
        '<>10',
        'v^0-1',
        '^v01',
        '>>-10',
        '>v-10',
        '>^-10',
        'vv0-1',
        'v<0-1',
        'v>0-1'
    ]
    cart_hash = dir1 + dir2 + str(x1-x2) + str(y1-y2)
    # print(cart_hash)
    if(cart_hash in crashes):
        return True
    return False


class Cart:
    def __init__(self, x, y, direction, track=[''], turn_queues={'^': '<^>', '<': 'v<^', 'v': '>v<', '>': '^>v'}):
        """ Create a new point at x, y """
        self.x = x
        self.y = y
        self.direction = direction
        self.road = track[self.y][self.x]
        self.track = track
        self.turn_queues = turn_queues

    def move(self):
        next_x = self.x
        next_y = self.y
        if(self.direction == '^'):
            next_y -= 1
        if(self.direction == 'v'):
            next_y += 1
        if(self.direction == '<'):
            next_x -= 1
        if(self.direction == '>'):
            next_x += 1
        straights = {'>-': '>', '^|': '^', '<-': '<', 'v|': 'v'}
        left_turns = {'^\\': '<', '</': 'v', 'v\\': '>', '>/': '^'}
        right_turns = {'^/': '>', '<\\': '^', 'v/': '<', '>\\': 'v'}
        intersections = {
            '^+': self.turn_queues['^'][0],
            '<+': self.turn_queues['<'][0],
            'v+': self.turn_queues['v'][0],
            '>+': self.turn_queues['>'][0],
        }
        turns = {**straights, **left_turns, **right_turns, **intersections}

        next_road = self.track[next_y][next_x]

        try:
            next_direction = turns[self.direction + next_road]
        except:
            raise Exception('Undefined road: ' + self.direction + next_road)

        # need to copy, assigning is not enough
        next_turns = self.turn_queues.copy()
        if next_road == '+':
            for k, v in self.turn_queues.items():
                next_turns.update({k: v[1:] + v[0]})

        return Cart(next_x, next_y, next_direction, self.track, next_turns)


main()


class TrackTests(unittest.TestCase):

    def test_track_is_cleared(self):
        track = clear_track(['/>\\', '^*v', '\\</'])
        self.assertEqual(track, ['/-\\', '|*|', '\\-/'],
                         'incorrect first turn in intersection')

    def test_cart_initialization(self):
        raw_track = ['***', '--<', '***']
        carts = parse_carts(raw_track, clear_track(raw_track))
        cart = list(carts)[0]
        self.assertEqual(cart.x, 2,
                         'incorrect x position parsed')
        self.assertEqual(cart.y, 1,
                         'incorrect y position parsed')


class IntersectionTests(unittest.TestCase):

    def test_cart_turns_left_first(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect first turn in intersection')

    def test_cart_does_not_turn_second(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect second turn in intersection')

    def test_cart_turns_right_third(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        cart = cart.move()
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect third turn in intersection')


class TurningRightTests(unittest.TestCase):

    def setUp(self):

        test_track = ['|/-',
                      '\\+\\',
                      '-/|']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_cart_moving_up_turns_right(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual(cart.direction, '>',
                         'incorrect right turn when moving up')

    def test_cart_moving_left_turns_right(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect right turn when moving left')

    def test_cart_moving_down_turns_right(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect right turn when moving down')

    def test_cart_moving_right_turns_right(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual(cart.direction, 'v',
                         'incorrect right turn when moving right')


class TurningLeftTests(unittest.TestCase):

    def setUp(self):

        test_track = ['-\\|',
                      '/+/',
                      '|\\-']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_cart_moving_up_turns_left(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect left turn when moving up')

    def test_cart_moving_left_turns_left(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual(cart.direction, 'v',
                         'incorrect left turn when moving left')

    def test_cart_moving_down_turns_left(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual(cart.direction, '>',
                         'incorrect left turn when moving down')

    def test_cart_moving_right_turns_left(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect left turn when moving right')


class MovingTests(unittest.TestCase):

    def setUp(self):

        test_track = ['*/-',
                      '-+-',
                      '*\\-']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_initial_road_detection(self):
        cart = self.test_cart(direction='^')
        self.assertEqual(
            cart.road, '+', 'incorrect initial road detection for ^')

    def test_cart_can_move_up(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (1, 0), 'incorrect up move')

    def test_cart_can_move_down(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (1, 2), 'incorrect down move')

    def test_cart_can_move_left(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (0, 1), 'incorrect left move')

    def test_cart_can_move_right(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (2, 1), 'incorrect right move')

    def test_road_is_updated_on_move(self):
        cart = self.test_cart(0, 1, '^', ['/', '^'])
        cart = cart.move()
        self.assertEqual(cart.road, '/', 'incorrect road update')


if __name__ == '__main__':
    unittest.main()
