import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    node = [int(n) for n in read_input_as_string_list()[0].split()]
    result = process_node(node)['metadata']
    print(result)
    return result


def process_node(node):
    if(len(node) == 0):
        return (0, 0)

    children_metadata = []
    size_of_children = 0
    while(get_child_count(node) > 0):
        next_child = trim_header(node)
        process_result = process_node(next_child)
        children_metadata.append(process_result['metadata'])
        size = process_result['size']
        size_of_children += size
        node = trim_processed_node_from_start(node, size)
        node[0] -= 1

    metadata_size = get_metadata_size(node)
    metadata = node[2:2 + metadata_size]

    size_with_children = 2 + size_of_children + metadata_size
    metadata_with_children = sum(metadata) + sum(children_metadata)
    return {'size': size_with_children, 'metadata': metadata_with_children}

def trim_header(node):
    return node[2:]

def trim_processed_node_from_start(node, size):
    return node[:2] + node[2 + size:]


def get_child_count(node):
    return node[0]


def get_metadata_size(node):
    return node[1]


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(main(), 47244, 'incorrect header')


if __name__ == '__main__':
    unittest.main()
