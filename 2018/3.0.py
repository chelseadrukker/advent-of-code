import collections
import itertools
import os
import re
import sys


def main():
    strings = read_input_as_string_list()
    positions_with_size = list(map(lambda i: re.search(
        '#[0-9]+ @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)', i).groups(), strings))
    positions_with_size_as_integers = list(
        map(lambda r: list(map(lambda n: int(n), r)), positions_with_size))
    edges = list(map(lambda r: (
        r[0], r[1], r[0] + r[2], r[1] + r[3]), positions_with_size_as_integers))
    lists_of_points = list(map(lambda r: list(itertools.product(
        range(r[0], r[2]), range(r[1], r[3]))), edges))
    all_points = list(itertools.chain.from_iterable(lists_of_points))
    print(
        len([item for item, count in collections.Counter(all_points).items() if count > 1]))


def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], '3.input.txt'))]


main()
