import collections
import itertools
import os
import re
import sys
import unittest


def main():
    foo = 7347
    powers = dict()
    for x in range(0, 300):
        for y in range(0, 300):
            powers.update( {(x, y) : fuel_level(x, y, foo)})
    max_power = ((-1,-1), 0)
    for x in range(1, 299):
        for y in range(1, 299):
            group_power = get_group_power(x, y, powers)
            if group_power > max_power[1]:
                max_power = ((x, y), group_power)  
    print(max_power)      

def get_group_power(x, y, powers):
    return powers[(x, y)] + powers[x-1, y-1] + powers[x, y-1] + powers[x+1, y-1] + powers[x-1, y] + powers[x+1, y] + powers[x-1, y+1] + powers[x, y+1] + powers[x+1, y+1]

def fuel_level(x, y, serial):
    rack_id = x + 10
    power_level = (((rack_id) * y) + serial) * rack_id
    return int(power_level / 100 % 10) - 5


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
