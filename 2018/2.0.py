import collections
import os
import sys


def main():
    ids = read_input_as_string_list()
    duo = triple = 0
    for id in ids:
        duo += 1 if contains_frequency(id, 2) else 0
        triple += 1 if contains_frequency(id, 3) else 0
    print(duo * triple)


def contains_frequency(id, val):
    return any(map(lambda c: c == val, count_char_frequency(id).values()))


def read_input_as_string_list():
    return [list(line.rstrip('\n')) for line in open(os.path.join(sys.path[0], '2.input.txt'))]


def count_char_frequency(text):
    return {c: text.count(c) for c in text}


main()
