import collections
import itertools
import os
import sys


def main():
    correct_hamming_distance = 1

    ids = read_input_as_string_list()
    combinations = itertools.combinations(ids, 2)
    hammings = map(lambda pair: (hamming(*pair), pair), combinations)
    correct_ids = dict(hammings)[correct_hamming_distance]
    without_different_characters = filter(lambda c: c[0] == c[correct_hamming_distance], zip(
        correct_ids[0], correct_ids[correct_hamming_distance]))
    result = ''.join(map(lambda c: c[0], without_different_characters))
    print(result)


def hamming(s1, s2):
    assert len(s1) == len(s2)
    return sum([c1 != c2 for c1, c2 in zip(s1, s2)])


def read_input_as_string_list():
    return [list(line.rstrip('\n')) for line in open(os.path.join(sys.path[0], '2.input.txt'))]


main()
