from approvaltests.reporters.generic_diff_reporter import GenericDiffReporter
from approvaltests.approvals import verify
from tile_factory import TileFactory
from tile import Tile
from board import Board
import constants as CONST
import unittest


class AllTests(unittest.TestCase):
    def setUp(self):
        self.reporter = GenericDiffReporter(
            ('TortoiseGitMerge', 'C:/Program Files/TortoiseGit/bin/TortoiseGitMerge.exe'))

    def test_elf_print(self):
        verify(str(TileFactory(3).create(CONST.ELF, 0, 0)), self.reporter)

    def test_goblin_print(self):
        verify(str(TileFactory(3).create(CONST.GOBLIN, 0, 0)), self.reporter)

    def test_goblin_dies(self):
        tiles = [TileFactory(3).create(CONST.ELF, 1, 0),
                 Tile(CONST.GOBLIN, 2, 0, 0)]
        verify(str(Board(tiles, TileFactory(3)).next_turn()), self.reporter)

    def test_target_does_not_get_attacked(self):
        tiles = [Tile(CONST.ELF, 1, 0, ad=15, attack_range=1),
                 Tile(CONST.GOBLIN, 3, 0, hp=10)]
        verify(str(Board(tiles, TileFactory(3)).next_turn()), self.reporter)

    def test_target_gets_attacked(self):
        tiles = [Tile(CONST.ELF, 1, 0, ad=15, attack_range=1),
                 Tile(CONST.GOBLIN, 2, 0, hp=10)]
        verify(str(Board(tiles, TileFactory(3)).next_turn()), self.reporter)

    def test_flagged_are_ignored(self):
        tiles = [Tile(CONST.GOBLIN, 0, 0, turn_made=True),
                 Tile(CONST.ELF, 1, 0, turn_made=True),
                 Tile(CONST.ELF, 2, 0, ad=15, attack_range=1),
                 Tile(CONST.GOBLIN, 3, 0, hp=10)]
        next_turn = Board(tiles, TileFactory(3)).next_turn()
        positions = str([str(tile.coordinate) for tile in next_turn.tiles])
        verify(str(next_turn) + '\n' + positions, self.reporter)

    def test_flags_are_not_clared_until_last_turn(self):
        tiles = [Tile(CONST.GOBLIN, 0, 0, turn_made=True),
                 Tile(CONST.ELF, 1, 0),
                 Tile(CONST.ELF, 2, 0)]
        next_turn = Board(tiles, TileFactory(3)).next_turn()
        verify(str([tile.turn_made for tile in next_turn.tiles]), self.reporter)

    def test_flags_are_clared_if_all_flagged(self):
        tiles = [Tile(CONST.GOBLIN, 0, 0, turn_made=True),
                 Tile(CONST.ELF, 1, 0, turn_made=True),
                 Tile(CONST.ELF, 2, 0, turn_made=True),
                 Tile(CONST.ELF, 3, 0, turn_made=True)]
        next_turn = Board(tiles, TileFactory(3)).next_turn()
        verify(str([tile.turn_made for tile in next_turn.tiles]), self.reporter)


if __name__ == '__main__':
    unittest.main()
