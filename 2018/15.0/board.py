from path_finder import find_path_to_nearest_target
import constants as CONST

class Board(object):
    def __init__(self, tiles, tile_factory, generation=0):
        self.tiles, x = self.reset_if_needed(tiles)
        self.tile_factory = tile_factory
        self.generation = generation + x

    def __str__(self):
        tiles = sorted(self.tiles, key=lambda tile: (
            tile.coordinate.y, tile.coordinate.x))
        width = max([tile.coordinate.x for tile in tiles]) + 1
        rows = chunks([str(tile) for tile in tiles], width)
        return '\n'.join([''.join(row) for row in rows])

    def next_turn(self):
        tiles = sorted(self.tiles, key=lambda tile: (
            tile.coordinate.y, tile.coordinate.x))

        attacker = self.next_active_creature(tiles)
        tiles.remove(attacker)

        target_type = CONST.GOBLIN if attacker.tile_type == CONST.ELF else CONST.ELF
        path = find_path_to_nearest_target(
            attacker, target_type, CONST.BLOCKING_TILES, tiles.copy())
        if(path == None):
            print('No path found')
            attacker.turn_made = True
            tiles.append(attacker)
            return Board(tiles, self.tile_factory, self.generation)
        if(len(path) > 2):
            destination = path[1]
            dt = next(tile for tile in tiles if tile.coordinate == destination)
            dt.coordinate = attacker.coordinate
            attacker.coordinate = destination

        targets_in_range = attacker.targets_in_range(tiles, target_type)
        targets_in_range = sorted(targets_in_range, key=lambda tile: (
            tile.hp, tile.coordinate.y, tile.coordinate.x))
        if(len(targets_in_range) > 0):
            target = targets_in_range[0]
            tiles.remove(target)
            attacker.attack(target)
            if(target.is_dead()):
                target = self.tile_factory.create(
                    CONST.EMPTY, target.coordinate.x, target.coordinate.y)
            tiles.append(target)

        attacker.turn_made = True

        tiles.append(attacker)

        return Board(tiles, self.tile_factory, self.generation)

    def next_active_creature(self, tiles):
        return next(tile for tile in tiles if (tile.is_creature() and not tile.turn_made))

    def reset_if_needed(self, tiles):
        if(self.no_turns_left(tiles)):
            for tile in tiles:
                tile.turn_made = False
            return tiles, 1
        return tiles, 0

    def no_turns_left(self, tiles):
        return next((tile for tile in tiles if (tile.is_creature() and not tile.turn_made)), None) == None

def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]