from coordinate import Coordinate
import constants

class Tile(object):
    def __init__(self, tile_type, x, y, hp=0, ad=0, attack_range=0, turn_made=False):
        self.tile_type = tile_type
        self.coordinate = Coordinate(x, y)
        self.hp = hp
        self.ad = ad
        self.attack_range = attack_range
        self.turn_made = turn_made

    def __str__(self):
        return self.tile_type
    
    def is_dead(self):
        return self.hp <= 0
    
    def attack(self, target):
        target.hp -= self.ad

    def is_in_range(self, target):
        b = self.coordinate
        t = target.coordinate
        if(b == t):
            raise Exception('Base and target can not be on same coordinate!')
        r = self.attack_range
        h_range = b.x == t.x and (abs(b.y - t.y) <= r)
        v_range = b.y == t.y and (abs(b.x - t.x) <= r)
        return h_range or v_range

    def is_creature(self):
        return self.tile_type in constants.CREATURE_TILES

    def targets_in_range(self, tiles, target_type):
        coordinates = self.coordinate.neighbours()
        return [tile for tile in tiles if tile.coordinate in coordinates and tile.tile_type == target_type]