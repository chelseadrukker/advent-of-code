from tile_factory import TileFactory
from board import Board
import constants as CONST
import os
import sys

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    rows = read_input_as_string_list()
    tile_factory = TileFactory(3)
    tiles = parse_tiles(rows, tile_factory)
    board = Board(tiles, tile_factory)
    while(not finished(board)):
        board = board.next_turn()
        # print(board)
        # print(board.generation, [tile.hp for tile in board.tiles if tile.tile_type in constants.CREATURE_TILES])
        print(board.generation *
              sum([tile.hp for tile in board.tiles if tile.tile_type in CONST.CREATURE_TILES]))


def finished(board):
    return CONST.GOBLIN not in str(board) or CONST.ELF not in str(board)


def parse_tiles(rows, tile_factory):
    tiles = []
    for y, row in enumerate(rows):
        for x, c in enumerate(row):
            tile = tile_factory.create(c, x, y)
            tiles.append(tile)
    return tiles


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()
