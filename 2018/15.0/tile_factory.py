import constants as CONST
from tile import Tile

class TileFactory(object):
    def __init__(self, elf_ad):
        self.elf_ad = elf_ad

    def create(self, tile_type, x, y):
        return {
            CONST.ELF: Tile(CONST.ELF, x, y, 200, self.elf_ad, 1),
            CONST.GOBLIN: Tile(CONST.GOBLIN, x, y, 200, 3, 1),
            CONST.WALL: Tile(CONST.WALL, x, y),
            CONST.EMPTY: Tile(CONST.EMPTY, x, y),
        }[tile_type]