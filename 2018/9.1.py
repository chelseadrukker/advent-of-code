import collections
import itertools
import os
import re
import sys
import unittest
from collections import deque

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}

def main():
    lines = read_input_as_string_list()
    for line in lines:
        groups = re.search('([0-9]+) .* ([0-9]+) .* ([0-9]+)', line).groups()
        calculate(int(groups[0]), int(groups[1]), groups[2])

def calculate(players, last_marble, result):
    marbles = deque([0])
    scores = {player: 0 for player in range(0, players)}
    for i in range(1, last_marble*100+1):
        if(i % 23 == 0):
            marbles.rotate(-7)
            scores[i%players] += marbles.pop() + i
        else:
            marbles.rotate(2)
            marbles.append(i)
    print(players, last_marble, result, ' ?= ', max(scores.values()))

def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
