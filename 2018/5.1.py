import collections
import itertools
import os
import re
import sys
import unittest


def main():
    string = read_input_as_string_list()[0]
    
    small_letters = [chr(c) for c in range(ord('a'), ord('z'))]
    large_letters = [chr(c) for c in range(ord('A'), ord('Z'))]
    for x in zip(small_letters, large_letters):
        temp = '%s' % string
        temp = temp.replace(x[0], '').replace(x[1], '')
        i = 0
        while(i < len(temp) - 1):
            if(is_pair(temp[i:i+2])):
                temp = temp[:i] + temp[i+2:]
                i -= i
            else:
                i += 1
        print(len(temp))

def flatten(nested):
    return list(itertools.chain.from_iterable(nested))

def is_pair(pair):
    return ord(pair[0]) + 32 == ord(pair[1]) or ord(pair[1]) + 32 == ord(pair[0])

def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], 'temp.txt'))]


main()

# Tests


class Tests(unittest.TestCase):
    def test_pair(self):
        actual = is_pair("aA")
        self.assertEqual(actual, True, 'incorrect pair detection')
        actual = is_pair("Bb")
        self.assertEqual(actual, True, 'incorrect pair detection')

if __name__ == '__main__':
    unittest.main() 
