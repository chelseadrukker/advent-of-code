import collections
import itertools
import os
import re
import sys
import unittest
from datetime import datetime

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}

start = datetime.now().second

def main():
    raw_track = read_input_as_string_list()
    clean_track = clear_track(raw_track)
    carts = parse_carts(raw_track, clean_track)
    game = Game(list(carts))
    tick = True
    while(tick):
        game = game.tick()
        print([(cart.x, cart.y, cart.direction) for cart in game.carts])
        positions = [(cart.x, cart.y) for cart in game.carts]
        crashes = [x for n, x in enumerate(positions) if x in positions[:n]]
        if len(crashes) > 0:
            print('Crash: ', crashes[0])
            tick = False


class Game:
    def __init__(self, carts):
        """ Create new game """
        self.carts = carts

    def tick(self):
        return Game([cart.move() for cart in self.carts])

def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines

def clear_track(raw_track):
    return [t.replace('^', '|').replace('v', '|').replace('<', '-').replace('>', '-') for t in raw_track]

def parse_carts(raw_track, clear_track):
    for y, row in enumerate(raw_track):
        for x, col in enumerate(row):
            if col in '<^>v':
                yield Cart(x, y, col, clear_track) #['<','^','>','v']

class Cart:
    def __init__(self, x, y, direction, track=[''], turn_queues={'^': '<^>', '<': 'v<^', 'v': '>v<', '>': '^>v'}):
        """ Create a new point at x, y """
        self.x = x
        self.y = y
        self.direction = direction
        self.road = track[self.y][self.x]
        self.track = track
        self.turn_queues = turn_queues

    def move(self):
        next_x = self.x
        next_y = self.y
        if(self.direction == '^'):
            next_y -= 1
        if(self.direction == 'v'):
            next_y += 1
        if(self.direction == '<'):
            next_x -= 1
        if(self.direction == '>'):
            next_x += 1
        straights = {'>-': '>', '^|': '^', '<-': '<', 'v|': 'v'}
        left_turns = {'^\\': '<', '</': 'v', 'v\\': '>', '>/': '^'}
        right_turns = {'^/': '>', '<\\': '^', 'v/': '<', '>\\': 'v'}
        intersections = {
            '^+': self.turn_queues['^'][0],
            '<+': self.turn_queues['<'][0],
            'v+': self.turn_queues['v'][0],
            '>+': self.turn_queues['>'][0],
        }
        turns = {**straights, **left_turns, **right_turns, **intersections}

        next_road = self.track[next_y][next_x]

        try:
            next_direction = turns[self.direction + next_road]
        except:
            raise Exception('Undefined road: ' + self.direction + next_road)

        # need to copy, assigning is not enough
        next_turns = self.turn_queues.copy()
        if next_road == '+':
            for k, v in self.turn_queues.items():
                next_turns.update({k: v[1:] + v[0]})

        return Cart(next_x, next_y, next_direction, self.track, next_turns)


main()

class TrackTests(unittest.TestCase):

    def test_track_is_cleared(self):
        track = clear_track(['/>\\', '^*v', '\\</'])
        self.assertEqual(track, ['/-\\', '|*|', '\\-/'],
                         'incorrect first turn in intersection')

    def test_cart_initialization(self):
        raw_track = ['***', '--<', '***']
        carts = parse_carts(raw_track, clear_track(raw_track))
        cart = list(carts)[0]
        self.assertEqual(cart.x, 2,
                         'incorrect x position parsed')
        self.assertEqual(cart.y, 1,
                         'incorrect y position parsed')


class IntersectionTests(unittest.TestCase):

    def test_cart_turns_left_first(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect first turn in intersection')

    def test_cart_does_not_turn_second(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect second turn in intersection')

    def test_cart_turns_right_third(self):
        cart = Cart(2, 2, '^', ['|**', '+++', '**|'])
        cart = cart.move()
        cart = cart.move()
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect third turn in intersection')


class TurningRightTests(unittest.TestCase):

    def setUp(self):

        test_track = ['|/-',
                      '\\+\\',
                      '-/|']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_cart_moving_up_turns_right(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual(cart.direction, '>',
                         'incorrect right turn when moving up')

    def test_cart_moving_left_turns_right(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect right turn when moving left')

    def test_cart_moving_down_turns_right(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect right turn when moving down')

    def test_cart_moving_right_turns_right(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual(cart.direction, 'v',
                         'incorrect right turn when moving right')


class TurningLeftTests(unittest.TestCase):

    def setUp(self):

        test_track = ['-\\|',
                      '/+/',
                      '|\\-']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_cart_moving_up_turns_left(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual(cart.direction, '<',
                         'incorrect left turn when moving up')

    def test_cart_moving_left_turns_left(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual(cart.direction, 'v',
                         'incorrect left turn when moving left')

    def test_cart_moving_down_turns_left(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual(cart.direction, '>',
                         'incorrect left turn when moving down')

    def test_cart_moving_right_turns_left(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual(cart.direction, '^',
                         'incorrect left turn when moving right')


class MovingTests(unittest.TestCase):

    def setUp(self):

        test_track = ['*/-',
                      '-+-',
                      '*\\-']

        def test_cart(x=1, y=1, direction='^', track=test_track):
            return Cart(x, y, direction, track)

        self.test_cart = test_cart

    def test_initial_road_detection(self):
        cart = self.test_cart(direction='^')
        self.assertEqual(
            cart.road, '+', 'incorrect initial road detection for ^')

    def test_cart_can_move_up(self):
        cart = self.test_cart(direction='^')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (1, 0), 'incorrect up move')

    def test_cart_can_move_down(self):
        cart = self.test_cart(direction='v')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (1, 2), 'incorrect down move')

    def test_cart_can_move_left(self):
        cart = self.test_cart(direction='<')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (0, 1), 'incorrect left move')

    def test_cart_can_move_right(self):
        cart = self.test_cart(direction='>')
        cart = cart.move()
        self.assertEqual((cart.x, cart.y), (2, 1), 'incorrect right move')

    def test_road_is_updated_on_move(self):
        cart = self.test_cart(0, 1, '^', ['/', '^'])
        cart = cart.move()
        self.assertEqual(cart.road, '/', 'incorrect road update')


if __name__ == '__main__':
    unittest.main()
