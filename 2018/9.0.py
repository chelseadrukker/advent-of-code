import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}

def main():
    lines = read_input_as_string_list()
    for line in lines:
        groups = re.search('([0-9]+) .* ([0-9]+) .* ([0-9]+)', line).groups()
        calculate(int(groups[0]), int(groups[1]), groups[2])

def calculate(players, last_marble, result):
    marbles = [0]
    current = 0
    scores = {player: 0 for player in range(0, players)}
    for i in range(1, last_marble+1):
        if(i % 23 == 0):
            current = current - 7
            if(current < 0):
                current = len(marbles) + current
            scores[i%players] += marbles[current]
            scores[i%players] += i
            del marbles[current]
        elif(current == len(marbles) - 1):
            current = 1
            marbles.insert(current, i)
        else:
            current += 2
            marbles.insert(current, i)
    print(players, last_marble, result, ' ?= ', max(scores.values()))

def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
