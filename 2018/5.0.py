import collections
import itertools
import os
import re
import sys
import unittest


def main():
    string = read_input_as_string_list()[0]
    i = 0
    while(i < len(string) - 1):
        if(is_pair(string[i:i+2])):
            string = string[:i] + string[i+2:]
            i -= i
        else:
            i += 1
    print(string)
    print(len(string))

def flatten(nested):
    return list(itertools.chain.from_iterable(nested))

def is_pair(pair):
    return ord(pair[0]) + 32 == ord(pair[1]) or ord(pair[1]) + 32 == ord(pair[0])

def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], '5.0.input.txt'))]


main()

# Tests


class Tests(unittest.TestCase):
    def test_pair(self):
        actual = is_pair("aA")
        self.assertEqual(actual, True, 'incorrect pair detection')
        actual = is_pair("Bb")
        self.assertEqual(actual, True, 'incorrect pair detection')

if __name__ == '__main__':
    unittest.main() 
