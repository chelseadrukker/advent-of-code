import collections
import itertools
import os
import re
import sys
import unittest


def main():
    foo = 7347
    powers = dict()
    for x in range(0, 300):
        for y in range(0, 300):
            powers.update({(x, y, 1): fuel_level(x, y, foo)})
    max_power = list(
        reversed(list(sorted(powers.items(), key=lambda kv: kv[1]))))[0]
    print(max_power)
    for size in [x for x in range(2, 301)]:
        print(size)
        for x in range(0, 300-size):
            for y in range(0, 300-size):
                group_power = get_group_power(x, y, size, powers)
                powers.update({(x, y, size): group_power})
                if group_power > max_power[1]:
                    max_power = ((x, y, size), group_power)
        print(max_power)


def get_group_power(x, y, size, powers):
    if(size % 2 == 0):
        components = even_size_square_components(size, x, y)
    else:
        components = odd_size_square_components(size, x, y)
    return sum([powers[c] for c in components])


def odd_size_square_components(size, x, y):
    top_left_size = size - 1
    components = [
        (x, y, top_left_size),
        (x + top_left_size, y + top_left_size, 1),
    ]
    components += get_bottom_components(top_left_size, x, y)
    components += get_right_components(top_left_size, x, y)
    # print(components)
    return components


def get_bottom_components(size, x, y):
    return [(x + p[0], y + p[1], 1) for p in itertools.product(range(0, size), range(size, size + 1))]


def get_right_components(size, x, y):
    return [(x + p[0], y + p[1], 1) for p in itertools.product(range(size, size + 1), range(0, size))]


def even_size_square_components(size, x, y):
    component_size = size / 2
    components = [
        (x, y, component_size),
        (x + component_size, y, component_size),
        (x, y + component_size, component_size),
        (x + component_size, y + component_size, component_size),
    ]
    return components


def fuel_level(x, y, serial):
    rack_id = x + 10
    power_level = (((rack_id) * y) + serial) * rack_id
    return int(power_level / 100 % 10) - 5


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
