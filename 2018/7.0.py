import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    strings = read_input_as_string_list()
    moves = [[g for g in re.search(
        '.*([A-Z]).*([A-Z]).*', s).groups()] for s in strings]
    ends = [m[1] for m in moves]
    starts = [m[0] for m in moves]
    available = list(sorted(set(starts) - set(ends)))
    result = next_state([], moves, available)
    print(result)


def next_state(processed, moves, available):
    print(processed)
    print(available)
    current = available[0]
    processed.append(current)
    available = available[1:]
    unlocked = [m[1]
                for m in moves if len(locker(m[1], processed, moves)) == 0]
    available = available + unlocked
    moves = [m for m in moves if m[0]
             not in processed and m[1] not in available]
    if(len(available) == 0):
        return current
    return current + next_state(processed, moves, sorted(available))


def locker(m1, processed, moves):
    return [y for y in [x for x in moves if x[1] == m1] if y[0] not in processed]


def read_input_as_string_list():
    return [line.rstrip('\n') for line in open(os.path.join(sys.path[0], CONFIG['input_file']))]


main()
