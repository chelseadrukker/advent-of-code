import sys
import os

frequencies = [int(line.rstrip('\n')) for line in open(os.path.join(sys.path[0], '1.input.txt'))]
print(sum(frequencies))