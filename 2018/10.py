import collections
import itertools
import os
import re
import sys
import unittest

CONFIG = {
    'input_file': os.path.basename(__file__).split('.')[0] + '.input.txt'
}


def main():
    lines = read_input_as_string_list()
    coordinates = []
    for line in lines:
        groups = re.search(
            '(-?[0-9]+).*?(-?[0-9]+).*?(-?[0-9]+).*?(-?[0-9]+)', line).groups()
        coordinates.append((int(groups[0]), int(
            groups[1]), int(groups[2]), int(groups[3])))
    sec=0
    cont = True
    while(cont):
        sec += 1
        coordinates = [(c[0] + c[2], c[1] + c[3], c[2], c[3])
                       for c in coordinates]
        ys = [c[1] for c in coordinates]
        distance = max(ys) - min(ys)
        print(distance)
        if max(ys) - min(ys) < 100:
            cont = False

    for i in range(0, 100):
        sec += 1
        coordinates = [(c[0] + c[2], c[1] + c[3], c[2], c[3])
                       for c in coordinates]
        positions = [(c[0], c[1]) for c in coordinates]
        xes = [c[0] for c in coordinates]
        ys = [c[1] for c in coordinates]
        result = ''
        for r in range(min(ys), max(ys)+1):
            for col in range(min(xes), max(xes)+1):
                result += '#' if (col, r) in positions else ' '
            result += '\n'
        
        print(result)
        print(sec)
        input()


def read_input_as_string_list():
    file = open(os.path.join(sys.path[0], CONFIG['input_file']))
    lines = [line.rstrip('\n') for line in file]
    file.close()
    return lines


main()


class Tests(unittest.TestCase):
    def test_header(self):
        self.assertEqual(True, True, 'incorrect test')


if __name__ == '__main__':
    unittest.main()
